package com.elezionicomunalionline.client.service;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;

public interface ClientInterface {

	void registraUtente(UtenteRegistrato utenteregistrato);
	
	void eliminaUtenti();
	
	void getUtenti();
	
	void getFunzionari();
	
	void login(String username, String password);
	
	void nominaFunzionario(UtenteRegistrato utenteregistrato);
	
	void revocaFunzionario(Funzionario funzionario);
		
	void creaElezione(Elezione elezione);
	
	void getElezioni();
	
	void eliminaElezioni();
	
	void creaListaElettorale(String elezione, String nome, String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati, UtenteRegistrato creatore);
	
	void approvaListaElettorale(ArrayList<ListaElettorale> listaElettorale);
	
	void revocaListaElettorale(ArrayList<ListaElettorale> listaElettorale);
	
	void getListeElettorali();
	
	void getElezioniUtente(UtenteRegistrato utente);
	
	void getSindacoElezione(String elezione);
	
	void eliminaListe();
	
	void getListeElettoraliPendenti();
	
	void getElezioniInCorso(String username);
	
	void getListeElettoraliElezione(String elezione);
	
	void getCandidatiLista(String nomeLista, String elezione);
	
	void votaConCandidato(String elezione, String lista, String candidato, String votante);
	
	void votaSenzaCandidato(String elezione, String lista, String votante);
	
	void getVoti();
	
	void getListeElettoraliCreatore(UtenteRegistrato utente);
	
	void getElezioniConcluse();
	
	void risultatiElezione(String oggettoElezione);
	
}
