package com.elezionicomunalionline.client.service;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Cittadino;
import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.Risultato;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("basicservice")
public interface StandardService extends RemoteService{
	
	boolean registraUtente(UtenteRegistrato utenteregistrato);
	
	void eliminaUtenti();
	
	ArrayList<UtenteRegistrato> getUtenti();
	
	ArrayList<Funzionario> getFunzionari();

	Cittadino login(String username, String password);
		
	boolean nominaFunzionario(UtenteRegistrato utenteregistrato);
	
	boolean revocaFunzionario(Funzionario funzionario);
	
	boolean creaElezione(Elezione elezione);
	
	ArrayList<Elezione> getElezioni();
	
	void eliminaElezioni();
	
	boolean creaListaElettorale(String elezione, String nome, String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati, UtenteRegistrato creatore);
	
	boolean approvaListaElettorale(ArrayList<ListaElettorale> listaElettorale);
	
	boolean revocaListaElettorale(ArrayList<ListaElettorale> listaElettorale);
	
	ArrayList<ListaElettorale> getListeElettorali();
	
    ArrayList<Elezione> getElezioniUtente(UtenteRegistrato utente);
    
    ArrayList<UtenteRegistrato> getSindacoElezione(String elezione);
    
    void eliminaListe();
	
    ArrayList<ListaElettorale> getListeElettoraliPendenti();
    
    ArrayList<Elezione> getElezioniInCorso(String username);
    
    ArrayList<ListaElettorale> getListeElettoraliElezione(String elezione);
    
    ArrayList<UtenteRegistrato> getCandidatiLista(String nomeLista, String elezione);
    
    boolean votaConCandidato(String elezione, String lista, String candidato, String votante);
    
    boolean votaSenzaCandidato(String elezione, String lista, String votante);
    
    ArrayList<Voto> getVoti();
    
    ArrayList<ListaElettorale> getListeElettoraliCreatore(UtenteRegistrato utente);
    
    ArrayList<Elezione> getElezioniConcluse();
    
    ArrayList<Risultato> risultatiElezione(String oggettoElezione);
    
}
