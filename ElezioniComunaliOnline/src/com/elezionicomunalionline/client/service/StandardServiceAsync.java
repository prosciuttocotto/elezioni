package com.elezionicomunalionline.client.service;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface StandardServiceAsync {
	
	void registraUtente(UtenteRegistrato utenteregistrato, AsyncCallback callback);
	
	void eliminaUtenti(AsyncCallback callback);
	
	void getUtenti(AsyncCallback callback);
	
	void getFunzionari(AsyncCallback callback);
	
	void login(String username, String password, AsyncCallback callback);
	
	void nominaFunzionario(UtenteRegistrato utenteregistrato, AsyncCallback callback);
	
	void revocaFunzionario(Funzionario funzionario, AsyncCallback callback);
		
	void creaElezione(Elezione elezione, AsyncCallback callback);
	
	void getElezioni(AsyncCallback callback);
	
	void eliminaElezioni(AsyncCallback callback);
	
	void creaListaElettorale(String elezione, String nome, String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati, UtenteRegistrato creatore, AsyncCallback callback);
	
	//void ammettiListaElettorale(ListaElettorale listaElettorale, AsyncCallback callback);
	
	void approvaListaElettorale(ArrayList<ListaElettorale> listaElettorale, AsyncCallback callback);
	
	void revocaListaElettorale(ArrayList<ListaElettorale> listaElettorale, AsyncCallback callback);
	
	void getListeElettorali(AsyncCallback callback);
	
	void getElezioniUtente(UtenteRegistrato utente, AsyncCallback callback);
	
	void getSindacoElezione(String elezione, AsyncCallback callback);
	
	void eliminaListe(AsyncCallback callback);
	
	void getListeElettoraliPendenti(AsyncCallback callback);
	
	void getElezioniInCorso(String username,AsyncCallback callback);
	
	void getListeElettoraliElezione(String elezione, AsyncCallback callback);
	
	void getCandidatiLista(String nomeLista, String elezione, AsyncCallback callback);
	
	void votaConCandidato(String elezione, String lista, String candidato, String votante, AsyncCallback callback);
	
	void votaSenzaCandidato(String elezione, String lista, String votante, AsyncCallback callback);
	
	void getVoti(AsyncCallback callback);
	
	void getListeElettoraliCreatore(UtenteRegistrato utente, AsyncCallback callback);
	
	void getElezioniConcluse(AsyncCallback callback); 
	
	void risultatiElezione(String oggettoElezione, AsyncCallback callback);
	
}
