package com.elezionicomunalionline.client.service;

import java.util.ArrayList;

import com.elezionicomunalionline.client.gui.Main;
import com.elezionicomunalionline.client.model.Admin;
import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.Risultato;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public class ClientImplementor implements ClientInterface {
	
	private StandardServiceAsync serviceAsync;
	private Main main;
	
	public ClientImplementor(String url) {
		serviceAsync = GWT.create(StandardService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) serviceAsync;
		endpoint.setServiceEntryPoint(url); // URL is the servlet url
		
		main = new Main(this);
	}
	
	public Main getMainGUI() {
		return main;
	}
	
	@Override
	public void registraUtente(UtenteRegistrato utenteRegistrato) {
		serviceAsync.registraUtente(utenteRegistrato, new SaveInDBCallback());
	}
	
	@Override
	public void eliminaUtenti() {
		serviceAsync.eliminaUtenti(new DefaultCallback());
	}
	
	@Override
	public void getUtenti() {
		serviceAsync.getUtenti(new UtentiCallback());
	}
	
	@Override
	public void getFunzionari() {
		serviceAsync.getFunzionari(new FunzionariCallback());
	}
	
	@Override
	public void login(String username, String password) {
		serviceAsync.login(username, password, new LoginCallback());
	}
	
	@Override
	public void nominaFunzionario(UtenteRegistrato utenteregistrato) {
		serviceAsync.nominaFunzionario(utenteregistrato, new NominaCallback());
	}
	
	@Override
	public void revocaFunzionario(Funzionario funzionario) {
		serviceAsync.revocaFunzionario(funzionario, new RevocaCallback());
	}
	
	@Override
	public void creaElezione(Elezione elezione) {
		serviceAsync.creaElezione(elezione, new SaveInDBCallback());
	}
	
	@Override
	public void getElezioni() {
		serviceAsync.getElezioni(new ElezioniCallback());
	}
	
	@Override
	public void eliminaElezioni() {
		serviceAsync.eliminaElezioni(new DefaultCallback());
	}
	
	@Override 
	public void creaListaElettorale(String elezione, String nome, String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati, UtenteRegistrato creatore) {
		serviceAsync.creaListaElettorale(elezione,nome,simbolo,sindaco,candidati, creatore, new SaveInDBCallback());
	}
	
	@Override
	public void approvaListaElettorale(ArrayList<ListaElettorale> listaElettorale) {
		serviceAsync.approvaListaElettorale(listaElettorale, new ApprovaListaCallback());
	}
	
	@Override
	public void revocaListaElettorale(ArrayList<ListaElettorale> listaElettorale) {
		serviceAsync.revocaListaElettorale(listaElettorale, new RevocaListaCallback());
	}
	
	@Override
	public void getListeElettorali() {
		serviceAsync.getListeElettorali(new ListeElettoraliCallback());
	}
	
	@Override
	public void getElezioniUtente(UtenteRegistrato utente) {
		serviceAsync.getElezioniUtente(utente,new ElezioniNuovaListaCallback());
	}
	
	@Override
	public void getSindacoElezione(String elezione) {
		serviceAsync.getSindacoElezione(elezione, new SindacoCallback());
	}
	
	@Override
	public void eliminaListe() {
		serviceAsync.eliminaListe(new DefaultCallback());
	}
	
	@Override
	public void getListeElettoraliPendenti() {
		serviceAsync.getListeElettoraliPendenti(new ListeElettoraliPendentiCallback());
	}
	
	@Override
	public void getElezioniInCorso(String username) {
		serviceAsync.getElezioniInCorso(username, new ElezioniInCorsoCallback());
	}
	
	@Override
	public void getListeElettoraliElezione(String elezione) {
		serviceAsync.getListeElettoraliElezione(elezione, new ListeElezioneCallback());
	}
	
	@Override
	public void getCandidatiLista(String nomeLista, String elezione){
		serviceAsync.getCandidatiLista(nomeLista,elezione, new CandidatiListaCallback());
	}
	
	@Override
	public void votaConCandidato(String elezione, String lista, String candidato, String votante) {
		serviceAsync.votaConCandidato(elezione, lista, candidato, votante, new SaveInDBCallback());
	}
	
	@Override
	public void votaSenzaCandidato(String elezione, String lista, String votante) {
		serviceAsync.votaSenzaCandidato(elezione, lista, votante, new SaveInDBCallback());
	}
	
	@Override
	public void getVoti(){
		serviceAsync.getVoti(new VotiCallback());
	}
	
	@Override
	public void getListeElettoraliCreatore(UtenteRegistrato utente) {
		serviceAsync.getListeElettoraliCreatore(utente, new getListeElettoraliCreatoreCallback());
	}
	
	@Override
	public void getElezioniConcluse(){
		serviceAsync.getElezioniConcluse(new ElezioniConcluseCallback());
	}
	
	@Override
	public void risultatiElezione(String oggettoElezione){
		serviceAsync.risultatiElezione(oggettoElezione, new RisultatiElezioniCallback());
	}
	
	//Callback
	
	private class DefaultCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			main.editSuccessLabel("Successo");
		}
	}
	
	private class SaveInDBCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean)
				if((boolean) result)
					main.editSuccessLabel("Elemento registrato!");
				else
					main.editSuccessLabel("Elemento gia' presente.");
		}
	}
	
	private class UtentiCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setListaUtenti((ArrayList<UtenteRegistrato>) result);
		}
	}
	
	
	private class FunzionariCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setListaFunzionari((ArrayList<Funzionario>) result);
		}
	}
	
	private class LoginCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof UtenteRegistrato) {
				main.loginUtente((UtenteRegistrato) result);
				main.editSuccessLabel("Login avvenuto!");
			} else if (result instanceof Admin) {
				main.loginUtente((Admin) result);
				main.editSuccessLabel("Login admin avvenuto!");
			} else if (result instanceof Funzionario) {
				main.loginUtente((Funzionario) result);
				main.editSuccessLabel("Login funzionario avvenuto!");
			}else {
				main.editErrorLabel("Username o password errati!");
			}
		}
	}
	
	private class NominaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean) {
				if((boolean) result) {
					getUtenti();
					getFunzionari();
				}
			}
		}
	}
	
	private class RevocaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean) {
				if((boolean) result) {
					getUtenti();
					getFunzionari();
				}
			}
		}
	}
	
	private class ElezioniCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setListaElezioni((ArrayList<Elezione>) result);
		}
	}
	
	private class ElezioniNuovaListaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setElezioneNuovaLista((ArrayList<Elezione>) result);
		}
	}
	
	private class ListeElettoraliCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>) {
				main.setListeElettorali((ArrayList<ListaElettorale>) result);
			}
		}
	}
	
	
	private class ListeElettoraliPendentiCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>) {
				main.setListeElettoraliPendenti((ArrayList<ListaElettorale>) result);
			}
		}
	}
	
	private class ApprovaListaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean) {
				if((boolean) result) {
					//getListeElettorali();
				}
			}
		}
	}
	
	private class RevocaListaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof Boolean) {
				if((boolean) result) {
					//getListeElettorali();
				}
			}
		}
	}
	
	private class SindacoCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>) {
				main.setSindacoNuovaLista((ArrayList<UtenteRegistrato>) result);
			}
		}
	}
	
	private class ElezioniInCorsoCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setListaElezioniInCorso((ArrayList<Elezione>) result);
		}
	}
	
	private class ListeElezioneCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setListeVotabili((ArrayList<ListaElettorale>) result);
		}
	}
	
	
	private class CandidatiListaCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>)
				main.setCandidatiVotabili((ArrayList<UtenteRegistrato>) result);
		}
	}
	
	private class VotiCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			
			if(result instanceof Boolean) {
				//
			}
		}
	}
	
	private class getListeElettoraliCreatoreCallback implements AsyncCallback<Object> {
	@Override
	public void onFailure(Throwable caught) {
		System.out.println("Errore");
	}
	
	@Override
	public void onSuccess(Object result) {
		if(result instanceof ArrayList<?>) {
			main.setListeElettoraliCreatore((ArrayList<ListaElettorale>) result);
			}
		}
	}
	
	private class ElezioniConcluseCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>) {
				main.setElezioniConcluse((ArrayList<Elezione>) result);
			}
		}
	}
	
	private class RisultatiElezioniCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			System.out.println("Errore");
		}

		@Override
		public void onSuccess(Object result) {
			if(result instanceof ArrayList<?>) {
				main.setRisultatiElezione((ArrayList<Risultato>) result);
			}
		}
	}
}
