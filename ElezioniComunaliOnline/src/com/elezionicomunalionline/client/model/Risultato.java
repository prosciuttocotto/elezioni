package com.elezionicomunalionline.client.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Risultato implements Serializable {
	private String nome;
	private int voti;
	
	public Risultato() {}
	
	public Risultato(String n,int v){
		this.nome=n;
		this.voti=v;
	}
	
	public String getNome() {return nome;}
	public int getVoti() {return voti;}
	
	public void setNome(String n) { nome= n; }
	public void setVoti(int v) { voti=v; }
	public void aumentaVoti() { voti++; }
	public void diminuisciVoti() { voti--; }
}

