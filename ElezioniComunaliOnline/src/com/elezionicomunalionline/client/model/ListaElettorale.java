package com.elezionicomunalionline.client.model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class ListaElettorale implements Serializable{

    private String nome, simbolo, stato;
    private UtenteRegistrato sindaco;
    private ArrayList<UtenteRegistrato> componentiLista = new ArrayList<UtenteRegistrato>();
    private Elezione elezione;
    private UtenteRegistrato creatoreLista; 
    
    public ListaElettorale() {	
    }
    
    public ListaElettorale(Elezione elezione,String nome,String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> componentiLista, UtenteRegistrato creatoreLista) {
   	 
     this.elezione = elezione;
   	 this.nome = nome;
   	 this.simbolo = simbolo;
   	 this.sindaco = sindaco;
   	 this.componentiLista.addAll(componentiLista);
   	 this.creatoreLista = creatoreLista;
   	 this.stato = "Pendente";
   	 
    }
    
    //Metodi Get
    public UtenteRegistrato getSindaco() { return this.sindaco; }
    public String getNome() { return nome; }
    public String getSimbolo() { return simbolo; }
    public String getStato() { return stato; }
    public ArrayList<UtenteRegistrato> getComponentiLista(){ return this.componentiLista; }
    public Elezione getElezione() {return elezione; }
    public UtenteRegistrato getCreatoreLista() { return creatoreLista;}
    
    //Metodi Set
    public void setSindaco(UtenteRegistrato sindaco) { this.sindaco = sindaco; }
    public void setNome(String nome) { this.nome = nome; }
    public void setSimbolo(String simbolo) { this.simbolo = simbolo; }
    public void setStato(String stato) { this.stato = stato; }
    
    
}