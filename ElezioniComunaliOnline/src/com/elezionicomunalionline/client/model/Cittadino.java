package com.elezionicomunalionline.client.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Cittadino implements Serializable {
	
	private String nome, cognome, telefono, email, codiceFiscale, indirizzo, documentoIdentita, numeroDoc, comuneDoc;
	private Date dataRilascio, dataScadenza;
	
	public Cittadino() {
	
	}
	
	public Cittadino(String nome, String cognome, String telefono, String email, String codiceFiscale, String indirizzo, 
			String documentoIdentita, String numeroDoc, String comuneDoc, Date dataRilascio, Date dataScadenza) {
		
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
		this.email = email;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.documentoIdentita = documentoIdentita;
		this.numeroDoc = numeroDoc;
		this.comuneDoc = comuneDoc;
		this.dataRilascio = dataRilascio;
		this.dataScadenza = dataScadenza;
		
	}
	
	//Metodi Get
	public String getNome() { return nome; }
	public String getCognome() { return cognome; }
	public String getTelefono() { return telefono; }
	public String getEmail() { return email; }
	public String getCodiceFiscale() { return codiceFiscale; }
	public String getIndirizzo() { return indirizzo; }
	public String getDocumentoIdendita() { return documentoIdentita; }
	public String getNumeroDoc() { return numeroDoc; }
	public String getComuneDoc() { return comuneDoc; }
	public Date getDataRilascio() { return dataRilascio; }
	public Date getDataScadenza() { return dataScadenza; }
	
	//Metodi Set
	public void setNome(String nome) { this.nome = nome; }
	public void setCognome(String cognome) { this.cognome = cognome; }
	public void setTelefono(String telefono) { this.telefono = telefono; }
	public void setEmail(String email) { this.email = email; }
	public void setCodiceFiscale(String codiceFiscale) { this.codiceFiscale = codiceFiscale; }
	public void setIndirizzo(String indirizzo) { this.indirizzo = indirizzo; }
	public void setDocumentIdentita(String documentoIdentita) { this.documentoIdentita = documentoIdentita; }
	public void setNumeroDoc(String numeroDoc) { this.numeroDoc = numeroDoc; }
	public void setComuneDoc(String comuneDoc) { this.comuneDoc = comuneDoc; }
	public void setDataRilascio(Date dataRilascio) { this.dataRilascio = dataRilascio; }
	public void setDataScadenza(Date dataScadenza) { this.dataScadenza = dataScadenza; }
}
