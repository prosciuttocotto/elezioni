package com.elezionicomunalionline.client.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Elezione implements Serializable{

	private Date dataInizio;
	private Date dataFine;
	private Date oraInizio;
	private Date oraFine;
	private String oggetto;
	
	public Elezione() {
		
	}
	
	public Elezione(String oggetto, Date dataInizio, Date dataFine) {
		
		this.oggetto = oggetto;
		this.dataInizio = dataInizio; 
		this.dataFine = dataFine;
		
	}
	
	public Elezione(Date dataInizio, Date dataFine, Date oraInizio, Date oraFine, String oggetto) {
		
		this.oggetto = oggetto;
		this.dataInizio = dataInizio; 
		this.dataFine = dataFine;
		this.oraInizio = oraInizio;
		this.oraFine = oraFine;
		
	}
	
	//Metodi Get
	public String getOggetto() { return oggetto; }
	public Date getDataInizio() { return dataInizio; }
	public Date getDataFine() { return dataFine; }
	public Date getOraInizio() { return oraInizio; }
	public Date getOraFine() { return oraFine; }

	//Metodi Set
	public void setOggetto(String oggetto) { this.oggetto = oggetto; }
	public void setDataInizio(Date dataInizio) { this.dataInizio = dataInizio; }
	public void setDataFine(Date dataFine) { this.dataFine = dataFine; }
	public void setOraInizio(Date oraInizio) { this.oraInizio = oraInizio; }
	public void setOraFine(Date oraFine) { this.oraFine = oraFine; }
	
}