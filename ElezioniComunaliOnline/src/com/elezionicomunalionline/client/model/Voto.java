package com.elezionicomunalionline.client.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Voto implements Serializable{
	// lista votata
	// (elezione a cui si riferisce)
	// candidato (facoltativo, c'� ma puo lasciarlo vuoto) 
	// chi ha fatto il voto (Utente Registrato o Funzionario) -> metto lo username
	
	
	
	//Interfaccia vota lista : Seleziona elezione (controllo che l'elezione sia ancora in corso)
	//                         Seleziona lista (controllo che la lista sia dell'elezione selezionata e sia Approvata)
	//                         Seleziona candidato
	//                         Vota
	
	private ListaElettorale listaVotata;
	private Elezione elezione;
	private UtenteRegistrato candidato = null;
	private String votante;
	
	public Voto() {}
	
	public Voto(ListaElettorale listaVotata,Elezione elezione,UtenteRegistrato candidato, String votante) {
		this.listaVotata= listaVotata;
		this.elezione = elezione;
		this.candidato = candidato;
		this.votante = votante;
	}
	
	public Voto(ListaElettorale listaVotata,Elezione elezione, String votante) {
		this.listaVotata= listaVotata;
		this.elezione = elezione;
		this.votante = votante;
	}
	
	
	public ListaElettorale getListaVotata() { return listaVotata;}
	public Elezione getElezione() {return elezione;}
	public UtenteRegistrato getCandidato() {return candidato;}
	public String getVotante() { return votante;}
	
}
