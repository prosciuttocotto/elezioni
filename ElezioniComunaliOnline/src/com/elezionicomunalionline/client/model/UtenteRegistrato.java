package com.elezionicomunalionline.client.model;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class UtenteRegistrato extends Cittadino implements Serializable {

	private String username, password;

	public UtenteRegistrato() {
		
	}
	
	public UtenteRegistrato(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public UtenteRegistrato(String username, String password, String nome, String cognome, String telefono, String email, String codiceFiscale, 
			String indirizzo, String documentoIdentita, String numeroDoc, String comuneDoc, Date dataRilascio, Date dataScadenza) {
		
			super(nome, cognome, telefono, email, codiceFiscale, indirizzo, documentoIdentita, numeroDoc, comuneDoc, dataRilascio, dataScadenza);
			this.username = username;
			this.password = password;
	}
	
	//Metodi Get
	public String getUsername() { return this.username; }
	public String getPassword() { return this.password; }
	
	//Metodi Set
	public void setUsername(String username) { this.username = username; }
	public void setPassword(String password) { this.password = password; }
	
}