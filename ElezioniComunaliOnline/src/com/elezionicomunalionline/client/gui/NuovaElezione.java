package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;
import java.util.Date;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.core.client.JsDate;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DatePicker;

public class NuovaElezione extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(5, 3);
	
	private ArrayList<Elezione> listaElezioni = new ArrayList<>();
	
	private TextBox oggettoTB;
	private DatePicker dataInizioDP;
	private ListBox oraInizioLB;
	private DatePicker dataFineDP;
	private ListBox oraFineLB;
	
	private Label errorLabel;
	private Label successLabel;
	private Label newNuovaElezione;
	
	private Button setListaElezioniBTN;
	Button eliminaElezioniBTN;
	
	private ClientImplementor clientImpl;

	public NuovaElezione(ClientImplementor clientImpl) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		
		Label oggettoLabel = new Label("Oggetto dell'elezione");
		oggettoTB = new TextBox();
		oggettoTB.getElement().setPropertyString("placeholder", "Oggetto");
		gridPanel.setWidget(0, 0, oggettoLabel);
		gridPanel.setWidget(0, 1, oggettoTB);
		
		Label inizioLabel = new Label("Data inizio");
		dataInizioDP = new DatePicker();
		dataInizioDP.setValue(new Date(), true);
		gridPanel.setWidget(1, 0, inizioLabel);
		gridPanel.setWidget(1, 1, dataInizioDP);
		
		oraInizioLB = new ListBox();
		for(double i = 8; i <= 14; i+=0.5)
			oraInizioLB.addItem((int)i + ":" + (i%1 == 0 ? "00" : "30"));
		gridPanel.setWidget(1, 2, oraInizioLB);
		
		Label fineLabel = new Label("Data fine");
		dataFineDP = new DatePicker();
		dataFineDP.setValue(new Date(), true);
		gridPanel.setWidget(2, 0, fineLabel);
		gridPanel.setWidget(2, 1, dataFineDP);
						
		oraFineLB = new ListBox();
		for(double i = 17; i <= 23; i+=0.5)
			oraFineLB.addItem((int)i + ":" + (i%1 == 0 ? "00" : "30"));
		gridPanel.setWidget(2, 2, oraFineLB);
		
		// Labels
		errorLabel = new Label("");
		successLabel = new Label("");
		
		// Buttons
		Button creaElezioneBTN = new Button("Crea elezione");
		creaElezioneBTN.addClickHandler(new CreaElezioneBTNClickHandler());
		gridPanel.setWidget(3, 0, creaElezioneBTN);
		
		setListaElezioniBTN = new Button("Stampa Elezioni");
		setListaElezioniBTN.addClickHandler(new setListaElezioniBTNClickHandler());
		
		eliminaElezioniBTN = new Button("Elimina Elezioni");
		eliminaElezioniBTN.addClickHandler(new eliminaElezioniBTNClickHandler());
		
		vPanel.add(gridPanel);
		vPanel.add(successLabel);
		vPanel.add(errorLabel);
		vPanel.add(setListaElezioniBTN);
		//vPanel.add(eliminaElezioniBTN);
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	//label in caso di successo
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	//label in caso di insuccesso
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	
	private void setLayout() {
		newNuovaElezione = new Label("Per inserire un'altra elezione, clicca sul pulsante Crea Elezione!");
		vPanel.remove(gridPanel);
		vPanel.remove(setListaElezioniBTN);
		vPanel.remove(eliminaElezioniBTN);
		vPanel.add(newNuovaElezione);
	}
	
	public void setListaElezioni(ArrayList<Elezione> elezioni) {
		VerticalPanel elez = new VerticalPanel();
		for(Elezione elezione : elezioni) {
			listaElezioni.add(elezione);
			elez.add(new Label(elezione.getOggetto() + " ")); 
		}
		gridPanel.setWidget(4, 0, elez);
	}
	
	private class setListaElezioniBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			clientImpl.getElezioni();
		}
	}
	
	private class eliminaElezioniBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			clientImpl.eliminaElezioni();
		}
	}
	
	private class CreaElezioneBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			Date oggi = new Date();
			DateTimeFormat dtf = DateTimeFormat.getFormat("d"); // d = day in month
			
			String oggetto = oggettoTB.getText();
			Date dataInizio = (Date) dataInizioDP.getValue();
			String oraInizio = oraInizioLB.getSelectedItemText();
			Date dataFine = (Date) dataFineDP.getValue();
			String oraFine = oraFineLB.getSelectedItemText();
			
			if(dataFine.compareTo(dataInizio) >= 0) {
				String data = dtf.format(dataInizio);
				String orario = String.valueOf(JsDate.create().getHours()) + ":" + String.valueOf(JsDate.create().getMinutes());
				if(oggetto.equals("")) {
					editErrorLabel("Non hai riempito tutti i campi!");
				} else if (dataInizio.before(oggi)) {
					if((Integer.parseInt(data)<JsDate.create().getDate()) || (Double.parseDouble(oraInizio)<Double.parseDouble(orario))) {
					editErrorLabel("La data/ora di inizio elezione deve essere dopo la data di oggi.");
					}else {
						Elezione elezione = new Elezione(oggetto, dataInizio, dataFine);
						clientImpl.creaElezione(elezione);
						setLayout();
					}
				} else {
					Elezione elezione = new Elezione(oggetto, dataInizio, dataFine);
					clientImpl.creaElezione(elezione);
					setLayout();
				}
			} else {
				editErrorLabel("La data di fine elezione deve essere dopo la data di inizio.");
			}
		}
	}

}
