package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class VotaLista extends Composite {
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(7, 2);
	
	private ClientImplementor clientImpl;
	private UtenteRegistrato utente = null;
	private Funzionario funzionario = null;
	
	private ListBox elezioneListBox = null;
	private ListBox listaListBox = null;
	private ListBox candidatiListBox = null;
	private Button selezionaLista = null;
	private Button selezioneCandidato = null;
	private Button votaLista = null;
	private boolean candidatoSelezionato = false;

	private Label labelVuoto = new Label();
	private Label error = null;
	
	
	public VotaLista(ClientImplementor clientImple, UtenteRegistrato utenteS) {
		initWidget(this.vPanel);
		this.clientImpl = clientImple;
		this.utente = utenteS;
		
		votaLista = new Button("Vota lista");
		votaLista.addClickHandler(new VotaClickHandler());
		
		clientImpl.getElezioniInCorso(utenteS.getUsername());
		vPanel.add(gridPanel);
	}
	
	public VotaLista(ClientImplementor clientImple, Funzionario funzionarioS) {
		initWidget(this.vPanel);
		this.clientImpl = clientImple;
		this.funzionario = funzionarioS;
		
		votaLista = new Button("Vota lista");
		votaLista.addClickHandler(new VotaClickHandler());
		
		clientImpl.getElezioniInCorso(funzionarioS.getUsername());
		vPanel.add(gridPanel);
	}
	
	public void setListaElezioniInCorso(ArrayList<Elezione> elezioni){
		Label labelElezioni = new Label("Elezione: ");
		elezioneListBox = new ListBox();
		for (Elezione e: elezioni) {
			elezioneListBox.addItem(e.getOggetto());
		}
		gridPanel.setWidget(0,0, labelElezioni);
		gridPanel.setWidget(0,1, elezioneListBox);
		
		selezionaLista = new Button("Seleziona Lista");
		selezionaLista.addClickHandler(new ListaClickHandler());
		gridPanel.setWidget(1, 0, selezionaLista);
	}
	
	public void setListeVotabili(ArrayList<ListaElettorale> listeVotabili) {
		if(listeVotabili.size()>0) {
			Label labelLista = new Label("Lista : ");
			listaListBox = new ListBox();
			
			for (ListaElettorale l: listeVotabili) {
				listaListBox.addItem(l.getNome());
			}
			gridPanel.setWidget(2,0, labelLista);
			gridPanel.setWidget(2,1, listaListBox);
			
			selezioneCandidato = new Button("Seleziona Candidato");
			selezioneCandidato.addClickHandler(new CandidatoClickHandler());
			gridPanel.setWidget(3, 0, selezioneCandidato);
			gridPanel.setWidget(3, 1, votaLista);
		}else {
			Label noListe = new Label("In questa elezione non ci sono liste!");
			gridPanel.setWidget(2,0, noListe);
		}
	}
	
	public void setCandidatiVotabili(ArrayList<UtenteRegistrato> utentiVotabili) {
		if(utentiVotabili.size()>0) {
			candidatoSelezionato = true;
			gridPanel.remove(votaLista);
			Label labelCandidato = new Label("Candidato : ");
			candidatiListBox = new ListBox();
			
			for (UtenteRegistrato u: utentiVotabili) {
				candidatiListBox.addItem(u.getUsername()+" "+u.getNome()+" "+u.getCognome());
			}
			gridPanel.setWidget(4,0, labelCandidato);
			gridPanel.setWidget(4,1, candidatiListBox);
			
			votaLista = new Button("Vota lista");
			votaLista.addClickHandler(new VotaClickHandler());
			gridPanel.setWidget(5, 0, votaLista);
		}else {
			Label noCandidato = new Label("Non ci sono candidati in questa Lista!");
			gridPanel.setWidget(4, 0, labelVuoto);
			gridPanel.setWidget(4, 1, labelVuoto);
			gridPanel.setWidget(5, 0, labelVuoto);
			gridPanel.setWidget(3, 1, votaLista);
			gridPanel.setWidget(4, 0, noCandidato);
		}
		
	}
	
	private class ListaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			String e = elezioneListBox.getSelectedItemText();
			gridPanel.setWidget(2, 0, labelVuoto);
			gridPanel.setWidget(2, 1, labelVuoto);
			gridPanel.setWidget(3, 0, labelVuoto);
			gridPanel.setWidget(3, 1, labelVuoto);
			gridPanel.setWidget(4, 0, labelVuoto);
			gridPanel.setWidget(4, 1, labelVuoto);
			gridPanel.setWidget(5, 0, labelVuoto);
			clientImpl.getListeElettoraliElezione(e);
		}
	}
	
	private class CandidatoClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			String l = listaListBox.getSelectedItemText();
			String e = elezioneListBox.getSelectedItemText();
			//labelVuoto
			clientImpl.getCandidatiLista(l,e);
		}
	}
	
	private class VotaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			if(error != null) {
				vPanel.remove(error);
			}
			String l = listaListBox.getSelectedItemText();
			String e = elezioneListBox.getSelectedItemText();
			String username;
			int conta = 0;
			if(utente != null) {
				username = utente.getUsername();
			}else {
				username = funzionario.getUsername();
			}

			if(l.equals("")) { conta++; }
			if(e.equals("")) { conta++; }
			if(username.equals("")) { conta++;}
			if(conta == 0) {
				if(candidatoSelezionato) {
					String c = candidatiListBox.getSelectedItemText();
					String[] part = c.split(" ");
					clientImpl.votaConCandidato(e, l, part[0], username);
				}else {
					clientImpl.votaSenzaCandidato(e, l, username);
				}
				Label nuovo = new Label("Per votare di nuovo, clicca sul pulsante Vota!");
				vPanel.remove(gridPanel);
				vPanel.add(nuovo);
			}else {
				error = new Label("Non hai inserito tutti i campi. Controlla!");
				vPanel.add(error);
			}
			
		}
	}
	
}


