package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

public class AmmettiLista extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(4, 2);
	
	private Label errorLabel;
	private Label successLabel;
	
	private ArrayList<ListaElettorale> listaListe = new ArrayList<>();
	private ArrayList<ListaElettorale> selezioneListe = new ArrayList<>();
	private ClientImplementor clientImpl;
	private Button approvaLista = null;
	private Button rigettaLista = null;
	
	private Label newNuovaApprova;
	
	public AmmettiLista (ClientImplementor clientImpl) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		
		clientImpl.getListeElettoraliPendenti();
		
		errorLabel = new Label("");
		successLabel = new Label("");
		
		vPanel.add(gridPanel);
		vPanel.add(successLabel);
		vPanel.add(errorLabel);
		
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	//label in caso di successo
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	//label in caso di insuccesso
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	
	//Lista di tutte le liste da ammettere
	public void setListeElettoraliPendenti(ArrayList<ListaElettorale> liste) {
			
		VerticalPanel listeCheckBoxes = new VerticalPanel();
		VerticalPanel datiListe = new VerticalPanel();
		Label listeLabel = new Label("LISTE DA AMMETTERE/REVOCARE");
		//Button ammettiButton = new Button("Ammetti");
		//ammettiButton.addClickHandler(new AmmettiClickHandler());
		
		int k = 0;
		for(ListaElettorale lista : liste) {
			CheckBox cb = new CheckBox();
			cb.setTitle(Integer.toString(k));
			listaListe.add(k, lista);
			cb.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					ListaElettorale listaSelezionata = listaListe.get(Integer.parseInt(((CheckBox)event.getSource()).getTitle()));
					if(((CheckBox)event.getSource()).getValue()) {
						selezioneListe.add(listaSelezionata);
					}else {
						selezioneListe.remove(listaSelezionata);
					}
				}
			});
			listeCheckBoxes.add(cb);
			datiListe.add(new Label(lista.getNome() + " - Sindaco: " +lista.getSindaco().getUsername()+ " " +lista.getSindaco().getNome()+ " "+lista.getSindaco().getCognome()));
			k++;
		}
		
		approvaLista= new Button("Approva Lista/e");
		approvaLista.addClickHandler(new ApprovaClickHandler());
		
		rigettaLista = new Button("Rigetta Lista/e");
		rigettaLista.addClickHandler(new RigettaClickHandler());
		
		gridPanel.setWidget(0, 0, listeLabel);
		gridPanel.setWidget(1, 0, listeCheckBoxes);
		gridPanel.setWidget(1, 1, datiListe);
		gridPanel.setWidget(2, 0, approvaLista);
		gridPanel.setWidget(2, 1, rigettaLista);
		
		if(datiListe.getWidgetCount()==0) {
			listeCheckBoxes.add(new Label("Nessuna lista da ammettere/revocare."));
			//ammettiButton.removeFromParent();
		}
	}
	
	private void setLayout() {
		newNuovaApprova = new Label("Per approvare o rigettare un'altra lista, clicca sul pulsante Ammetti Liste!");
		vPanel.remove(gridPanel);
		vPanel.add(newNuovaApprova);
	}
	
	
	private class ApprovaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			clientImpl.approvaListaElettorale(selezioneListe);
			selezioneListe.clear();
			setLayout();
			
		}
	}
	
	private class RigettaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			clientImpl.revocaListaElettorale(selezioneListe);
			selezioneListe.clear();
			setLayout();
		}
	}

}
