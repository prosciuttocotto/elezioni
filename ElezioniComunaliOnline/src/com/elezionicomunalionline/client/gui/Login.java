package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

public class Login extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(3, 2);
	
	private TextBox usernameBox;
	private PasswordTextBox passwordBox;
	
	private Label errorLabel;
	private Label successLabel;
	
	private ClientImplementor clientImpl;
	
	public Login(ClientImplementor clientImpl) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		
		//Box username
		Label username = new Label("Username: ");
		usernameBox = new TextBox();
		usernameBox.getElement().setPropertyString("placeholder", "Username");
		gridPanel.setWidget(0, 0, username);
		gridPanel.setWidget(0, 1, usernameBox);
		
		//Box password
		Label password = new Label("Password: ");
		passwordBox = new PasswordTextBox();
		passwordBox.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(1, 0, password);
		gridPanel.setWidget(1, 1, passwordBox);
		
		//Labels
		errorLabel = new Label("");
		successLabel = new Label("");
		
		//Button
		Button loginButton = new Button("Login");
		loginButton.addClickHandler(new LoginClickHandler());
		gridPanel.setWidget(2, 0, loginButton);
		
		vPanel.add(gridPanel);
		vPanel.add(successLabel);
		vPanel.add(errorLabel);
		
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	//label in caso di successo
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	//label in caso di insuccesso
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	
	private void setLayout() {
		vPanel.remove(gridPanel);
	}
	
	private class LoginClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			
			String username = usernameBox.getText();
			String password = passwordBox.getText();
			
			//controlla che tutti i campi siano pieni
			String err = "";
			ArrayList<String> controllaCampi = new ArrayList<>();
			if(username.equals("")) {	controllaCampi.add("username");	}
			if(password.equals("")) {	controllaCampi.add("password");	}
			if(controllaCampi.size() > 0) {
				err += "Non hai riempito tutti i campi!";
			}else {
				clientImpl.login(username, password);
				setLayout();
			}
			editErrorLabel(err);
			
		}
			
	}
	
}