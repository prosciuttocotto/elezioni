package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;
import java.util.Date;

import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DatePicker;

public class Registrazione extends Composite {
	
	private Grid gridPanel = new Grid(15, 2);
	private VerticalPanel vPanel = new VerticalPanel();
	
	private TextBox usernameTB;
	private TextBox nomeTB;
	private TextBox cognomeTB;
	private TextBox telefonoTB;
	private PasswordTextBox pwdTB;
	private TextBox emailTB;
	private TextBox codiceFiscaleTB;
	private TextBox indirizzoTB;
	// documento d'identita'
	private ListBox tipoDocTB; // tipo documento d'identita'
	private TextBox numeroDocTB; //numero documento d'identita'
	private TextBox comuneDocTB; // da chi e' rilasciato
	private DatePicker dRilascioDocTB; //da di rilascio del documento
	private DatePicker dScadenzaDocTB; // data di scadenza del documento
	
	private Label errorLabel;
	private Label successLabel;
	private Label newLabel;
	
	private ClientImplementor clientImpl;
	
	public Registrazione(ClientImplementor clientImpl) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		
		Label lbl1 = new Label("Username: ");
		usernameTB = new TextBox();
		usernameTB.getElement().setPropertyString("placeholder", "Username");
		gridPanel.setWidget(0, 0, lbl1);
		gridPanel.setWidget(0, 1, usernameTB);
		
		Label lbl2 = new Label("Nome: ");
		nomeTB = new TextBox();
		nomeTB.getElement().setPropertyString("placeholder", "Nome");
		gridPanel.setWidget(1, 0, lbl2);
		gridPanel.setWidget(1, 1, nomeTB);
		
		Label lbl3 = new Label("Cognome: ");
		cognomeTB = new TextBox();
		cognomeTB.getElement().setPropertyString("placeholder", "Cognome");
		gridPanel.setWidget(2, 0, lbl3);
		gridPanel.setWidget(2, 1, cognomeTB);
		
		Label lbl4 = new Label("Telefono: ");
		telefonoTB = new TextBox();
		telefonoTB.getElement().setPropertyString("placeholder", "Numero telefono");
		gridPanel.setWidget(3, 0, lbl4);
		gridPanel.setWidget(3, 1, telefonoTB);
		
		Label lbl5 = new Label("Password: ");
		pwdTB = new PasswordTextBox();
		pwdTB.getElement().setPropertyString("placeholder", "Password");
		gridPanel.setWidget(4, 0, lbl5);
		gridPanel.setWidget(4, 1, pwdTB);
		
		Label lbl6 = new Label("Email: ");
		emailTB = new TextBox();
		emailTB.getElement().setPropertyString("placeholder", "Email");
		gridPanel.setWidget(5, 0, lbl6);
		gridPanel.setWidget(5, 1, emailTB);
		
		Label lbl7 = new Label("Codice Fiscale: ");
		codiceFiscaleTB = new TextBox();
		codiceFiscaleTB.getElement().setPropertyString("placeholder", "Codice fiscale");
		gridPanel.setWidget(6, 0, lbl7);
		gridPanel.setWidget(6, 1, codiceFiscaleTB);
		
		Label lbl8 = new Label("Indirizzo: ");
		indirizzoTB = new TextBox();
		indirizzoTB.getElement().setPropertyString("placeholder", "Indirizzo");
		gridPanel.setWidget(7, 0, lbl8);
		gridPanel.setWidget(7, 1, indirizzoTB);
		
		Label lbl9 = new Label("Documento d'identita': ");
		gridPanel.setWidget(8, 0, lbl9);
		
		Label lbl10 = new Label("Tipo: ");
		tipoDocTB = new ListBox();
		tipoDocTB.addItem("Carta d'identita'");
		tipoDocTB.addItem("Passaporto");
		gridPanel.setWidget(9, 0, lbl10);
		gridPanel.setWidget(9, 1, tipoDocTB);
		
		Label lbl11 = new Label("Numero: ");
		numeroDocTB = new TextBox();
		numeroDocTB.getElement().setPropertyString("placeholder", "Numbero documento");
		gridPanel.setWidget(10, 0, lbl11);
		gridPanel.setWidget(10, 1, numeroDocTB);
		
		Label lbl12 = new Label("Comune di rilascio: ");
		comuneDocTB = new TextBox();
		comuneDocTB.getElement().setPropertyString("placeholder", "Comune rilascio documento");
		gridPanel.setWidget(11, 0, lbl12);
		gridPanel.setWidget(11, 1, comuneDocTB);
		
		Label lbl13 = new Label("Data rilascio: ");
		dRilascioDocTB = new DatePicker();
		dRilascioDocTB.setValue(new Date(), true);
		gridPanel.setWidget(12, 0, lbl13);
		gridPanel.setWidget(12, 1, dRilascioDocTB);
		
		Label lbl14 = new Label("Data scadenza: ");
		dScadenzaDocTB = new DatePicker();
		dScadenzaDocTB.setValue(new Date(), true);
		gridPanel.setWidget(13, 0, lbl14);
		gridPanel.setWidget(13, 1, dScadenzaDocTB);
		
		//labels
		errorLabel = new Label("");
		successLabel = new Label("");
		
		//button
		Button registraBTN = new Button("Registrati");
		registraBTN.addClickHandler(new RegistraClickHandler());
		gridPanel.setWidget(14, 0, registraBTN);
		
		vPanel.add(gridPanel);
		vPanel.add(successLabel);
		vPanel.add(errorLabel);
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	//label in caso di successo
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	//label in caso di insuccesso
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	
	private void setLayout() {
		newLabel = new Label("Utente registrato, per loggarti clicca sul pulsante Login!");
		vPanel.remove(gridPanel);
		vPanel.add(newLabel);
	}
	
	private class RegistraClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			
			Date oggi = new Date();
			
			String username = usernameTB.getText();
			String password = pwdTB.getText();
			String nome = nomeTB.getText();
			String cognome = cognomeTB.getText();
			String telefono = telefonoTB.getText();
			String email = emailTB.getText();
			String codiceFiscale = codiceFiscaleTB.getText();
			String indirizzo = indirizzoTB.getText();
			String tipoDoc = tipoDocTB.getSelectedItemText();
			String numeroDoc = numeroDocTB.getText();
			String comuneDoc = comuneDocTB.getText();
			Date dRilascioDoc = (Date) dRilascioDocTB.getValue();
			Date dScadenzaDoc = (Date) dScadenzaDocTB.getValue(); 
			
			//controlla che tutti i campi siano pieni
			String err = "";
			ArrayList<String> controllaCampi = new ArrayList<>();
			if(username.equals("")) {	controllaCampi.add("username");	}
			if(password.equals("")) {	controllaCampi.add("password");	}
			if(nome.equals("")) {	controllaCampi.add("nome");	}
			if(cognome.equals("")) {	controllaCampi.add("cognome");	}
			if(telefono.equals("")) {	controllaCampi.add("telefono");	}
			if(email.equals("")) {	controllaCampi.add("email");	}
			if(codiceFiscale.equals("")) {	controllaCampi.add("codiceFiscale");	}
			if(indirizzo.equals("")) {	controllaCampi.add("indirizzo");	}
			if(tipoDoc.equals("")) {	controllaCampi.add("tipoDoc");	}
			if(numeroDoc.equals("")) {	controllaCampi.add("numeroDoc");	}
			if(comuneDoc.equals("")) {	controllaCampi.add("comuneDoc");	}
			if(dRilascioDoc.equals("")) {	controllaCampi.add("dataRilascio");	}
			if(dScadenzaDoc.equals("")) {	controllaCampi.add("dataScadenza");	}
			
			if (dScadenzaDoc.before(oggi)) {
				err += "La data di scadenza documento deve essere dopo la data di oggi.";
			} else if (dRilascioDoc.after(oggi)) {
				err += "La data di rilascio documento deve essere prima della data di oggi.";
			} else if(controllaCampi.size() > 0) {
				err += "Non hai riempito tutti i campi!";
			}else {
				UtenteRegistrato utenteRegistrato = new UtenteRegistrato(username, password, nome, cognome, telefono, email, codiceFiscale,
						indirizzo, tipoDoc, numeroDoc, comuneDoc, dRilascioDoc, dScadenzaDoc);
				clientImpl.registraUtente(utenteRegistrato);
				setLayout();
			}
			editErrorLabel(err);
		}
			
	}
	

}