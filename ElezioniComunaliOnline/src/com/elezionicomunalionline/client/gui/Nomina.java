package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

public class Nomina extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(7, 7);
	
	private ArrayList<UtenteRegistrato> listaUtenti = new ArrayList<>();
	private ArrayList<UtenteRegistrato> selezioneUtenti = new ArrayList<>();
	
	private ArrayList<Funzionario> listaFunzionari = new ArrayList<>();
	private ArrayList<Funzionario> selezioneFunzionari = new ArrayList<>();
	
	private Label errorLabel;
	private Label successLabel;
	
	private ClientImplementor clientImpl;
	
	public Nomina(ClientImplementor clientImpl) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
				
		//Labels
		errorLabel = new Label("");
		successLabel = new Label("");
		
		vPanel.add(gridPanel);
		vPanel.add(successLabel);
		vPanel.add(errorLabel);
		
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	//label in caso di successo
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	//label in caso di insuccesso
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	
	public void onUpdateForeground(UtenteRegistrato sessioneUtente) {
		clientImpl.getUtenti();
		clientImpl.getFunzionari();
	}
	
	
	//Lista di tutti gli utenti registrati
	public void setListaUtenti(ArrayList<UtenteRegistrato> utenti) {
		
		VerticalPanel utentiCheckBoxes = new VerticalPanel();
		VerticalPanel datiUtenti = new VerticalPanel();
		Label utentiLabel = new Label("UTENTI");
		Button nominaButton = new Button("Nomina");
		nominaButton.addClickHandler(new NominaClickHandler());
		
		int k = 0;
		for(UtenteRegistrato utenteregistrato : utenti) {
			CheckBox cb = new CheckBox();
			cb.setTitle(Integer.toString(k));
			listaUtenti.add(k, utenteregistrato);
			cb.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					UtenteRegistrato utenteSelezionato = listaUtenti.get(Integer.parseInt(((CheckBox)event.getSource()).getTitle()));
					if(((CheckBox)event.getSource()).getValue()) {
						selezioneUtenti.add(utenteSelezionato);
					}else {
						selezioneUtenti.remove(utenteSelezionato);
					}
				}
			});
			utentiCheckBoxes.add(cb);
			datiUtenti.add(new Label(utenteregistrato.getUsername() + " " +utenteregistrato.getNome() + " " 
			+ utenteregistrato.getCognome() )); 
			k++;
		}
		gridPanel.setWidget(0, 0, utentiLabel);
		gridPanel.setWidget(1, 0, utentiCheckBoxes);
		gridPanel.setWidget(1, 1, datiUtenti);
		gridPanel.setWidget(2, 0, nominaButton);
		
		if(datiUtenti.getWidgetCount()==0) {
			utentiCheckBoxes.add(new Label("Nessun utente registrato presente."));
			nominaButton.removeFromParent();
		}
	}
	
	//Lista di tutti gli utenti registrati
	public void setListaFunzionari(ArrayList<Funzionario> funzionari) {
		
		VerticalPanel funzionariCheckBoxes = new VerticalPanel();
		VerticalPanel datiFunzionari = new VerticalPanel();
		Label funzionariLabel = new Label("FUNZIONARI");
		Button revocaButton = new Button("Revoca");
		revocaButton.addClickHandler(new RevocaClickHandler());
		
		int k = 0;
		for(Funzionario funzionario : funzionari) {
			CheckBox cb = new CheckBox();
			cb.setTitle(Integer.toString(k));
			listaFunzionari.add(k, funzionario);
			cb.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					Funzionario funzionarioSelezionato = listaFunzionari.get(Integer.parseInt(((CheckBox)event.getSource()).getTitle()));
					if(((CheckBox)event.getSource()).getValue()) {
						selezioneFunzionari.add(funzionarioSelezionato);
					}else {
						selezioneFunzionari.remove(funzionarioSelezionato);
					}
				}
			});
			funzionariCheckBoxes.add(cb);
			datiFunzionari.add(new Label(funzionario.getUsername() + " " +funzionario.getNome() + " " 
			+ funzionario.getCognome() ));
			k++;
		}
		gridPanel.setWidget(4, 0, funzionariLabel);
		gridPanel.setWidget(5, 0, funzionariCheckBoxes);
		gridPanel.setWidget(5, 1, datiFunzionari);
		gridPanel.setWidget(6, 0, revocaButton);
		
		if(datiFunzionari.getWidgetCount()==0) {
			funzionariCheckBoxes.add(new Label("Nessun funzionario presente."));
			revocaButton.removeFromParent();
		}
	}
	
	
	
	private class NominaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			for(UtenteRegistrato utenteregistrato : selezioneUtenti) {
				//l'utente selezionato diventa Funzionario
				clientImpl.nominaFunzionario(utenteregistrato);
				editSuccessLabel("Utente nominato correttamente");
				//per ogni utente selezionato (arraylist selezioneUtenti) creo un nuovo funzionario,
				//lo inserisco nell'arraylist listaFunzionari
				//e elimino l'utente registrato col suo stesso username
			}		
			selezioneUtenti.clear();
		}
	}
	
		
	private class RevocaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			resetLabels();
			for(Funzionario funzionario : selezioneFunzionari) {
				//l'utente selezionato ritorna UtenteRegistrato
				clientImpl.revocaFunzionario(funzionario);
				editSuccessLabel("Funzionario revocato correttamente");
				//per ogni funzionario selezionato (arraylist selezioneFunzionari) creo un nuovo utenteRegistrato,
				//lo inserisco nell'arraylist listaUtenti
				//e elimino il funzionario col suo stesso username
			}			
			selezioneUtenti.clear();
		}
	}
}