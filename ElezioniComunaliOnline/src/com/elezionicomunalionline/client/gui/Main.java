package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Admin;
import com.elezionicomunalionline.client.model.Cittadino;
import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.Risultato;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;

public class Main extends Composite{
	
	//struttura
	private VerticalPanel wrapper = new VerticalPanel();
	private VerticalPanel navbarPanel = new VerticalPanel();
	public VerticalPanel contentPanel = new VerticalPanel();
	
	//labels
	private Label errorLabel = null;
	private Label successLabel = null;
	private Label logoutLabel = null;
	
	//pulsanti
	Button VisualizzaElezioni = null;
	Button Registrazione = null;
	Button Login = null;
	Button Logout = null;
	Button NuovaLista = null;
	Button Vota = null;
	Button Profilo = null;
	Button AmmettiLista = null;
	Button Nomina = null;
	Button NuovaElezione = null;
	
	//sezioni
	private Registrazione registrazione = null;
	private Login login = null;
	private Profilo profilo = null;
	private Nomina nomina = null;
	private NuovaElezione nuovaelezione= null;
	private NuovaLista nuovalista = null;
	private AmmettiLista ammettilista = null;
	private VotaLista votalista = null;
	private Risultati risultati = null;
		
	//Client Implementor
	private ClientImplementor clientImpl;
	
	//Stato utente
	private boolean isAdmin = false;
	public UtenteRegistrato sessioneUtente = null;
	public Funzionario sessioneFunzionario = null;
	
	public Main(ClientImplementor clientImpl) {
		initWidget(wrapper);
		wrapper.add(navbarPanel);
		wrapper.add(contentPanel);
		this.clientImpl = clientImpl;
		
		impostaMenu();
		
		//Labels
		errorLabel = new Label("");
		successLabel = new Label("");
		
	}
	
	//label in caso di successo
	public void editSuccessLabel(String txt) {
		successLabel = new Label();
		successLabel.setText(txt);
		contentPanel.add(successLabel);
	}
	//label in caso di insuccesso
	public void editErrorLabel(String txt) {
		errorLabel = new Label();
		errorLabel.setText(txt);
		contentPanel.add(errorLabel);
	}
	
	//Richiama il metodo relativo alla lista di utenti registrati
	public void setListaUtenti(ArrayList<UtenteRegistrato> utenti) {
		if(nomina != null) {
			nomina.setListaUtenti(utenti);
		}
	}
	
	//Richiama il metodo relativo alla lista di funzionari
	public void setListaFunzionari(ArrayList<Funzionario> funzionari) {
		if(nomina != null) {
			nomina.setListaFunzionari(funzionari);
		}
	}
	
	//Richiama il metodo relativo alla lista di elezioni
	public void setListaElezioni(ArrayList<Elezione> elezioni) {
		if(nuovaelezione != null) {
			nuovaelezione.setListaElezioni(elezioni);
		}
	}
	
	public void setElezioneNuovaLista(ArrayList<Elezione> elezioni) {
		if(nuovalista != null) {
			nuovalista.setElezioneNuovaLista(elezioni);
		}
	}
	
	//Richiama il metodo relativo alla lista di elezioni
	public void setListeElettorali(ArrayList<ListaElettorale> liste) {
		if(nuovalista != null) {
			nuovalista.setListeElettorali(liste);
		}
	}
	
	public void setListeElettoraliPendenti(ArrayList<ListaElettorale> liste) {
		if(ammettilista != null) {
			ammettilista.setListeElettoraliPendenti(liste);
		}
	}
	
	
	//Richiama il metodo relativo alla lista dei sindaci
	public void setSindacoNuovaLista(ArrayList<UtenteRegistrato> utenti) {
		if(nuovalista != null) {
			nuovalista.setSindacoNuovaLista(utenti);
		}
	}
	
	// Richiama il metodo relativo alla lista delle elezioni in corso
	public void setListaElezioniInCorso(ArrayList<Elezione> elezioni) {
		if(votalista != null) {
			votalista.setListaElezioniInCorso(elezioni);
		}
	}
	
	// Richiama il metodo relativo alla lista delle liste votabili
	public void setListeVotabili(ArrayList<ListaElettorale> listeElettorali) {
		if(votalista != null) {
			votalista.setListeVotabili(listeElettorali);
		}
	}
	
	// Richiama il metodo relativo ai candidati votabili
	public void setCandidatiVotabili(ArrayList<UtenteRegistrato> utenti) {
		if(votalista != null) {
			votalista.setCandidatiVotabili(utenti);
		}
	}
	
	// Richiama il metodo relativo ai creatori delle liste elettorali
	public void setListeElettoraliCreatore(ArrayList<ListaElettorale> liste) {
		  if(profilo != null) {
			  profilo.setListeElettoraliCreatore(liste);
		  }
	}
	
	// Richiama il metodo relativo alle elezioni concluse
	public void setElezioniConcluse(ArrayList<Elezione> elezioniConcluse) {
		if(risultati !=null ) {
			risultati.setElezioniConcluse(elezioniConcluse);
		}
	}
	
	//Richiama il metodo relativo ai risultati 
	public void setRisultatiElezione(ArrayList<Risultato> risultat) {
		if(risultati != null) {
			risultati.setRisultatiElezione(risultat);
		}
	}
	
	//Controlla il tipo di utente
	public void loginUtente(Cittadino cittadino) {
		if(cittadino instanceof UtenteRegistrato) {
			sessioneUtente = (UtenteRegistrato) cittadino;
			sessioneFunzionario = null;
			isAdmin = false;
		} else if(cittadino instanceof Admin) {
			sessioneUtente = null;
			sessioneFunzionario = null;
			isAdmin = true;
		} else if(cittadino instanceof Funzionario){
			sessioneUtente = null;
			sessioneFunzionario = (Funzionario) cittadino;
			isAdmin = false;
		}else {
			sessioneUtente = null;
			sessioneFunzionario = null;
			isAdmin = false;
		}
		impostaMenu();
	}
	
	//Imposta i pulsanti che puo' utilizzare l'utente
	private void impostaMenu() {
		if(navbarPanel.getWidgetCount() > 0)
			for(int i = 0; i < navbarPanel.getWidgetCount(); i++)
				navbarPanel.remove(i);
		
		HorizontalPanel hPanel = new HorizontalPanel();
		
		VisualizzaElezioni = new Button("Risultati Elezioni");
		VisualizzaElezioni.setTitle("RisultatiBTN");
		VisualizzaElezioni.addClickHandler(new ButtonClickHandler());
		hPanel.add(VisualizzaElezioni);
		
		if(!isAdmin && sessioneUtente == null && sessioneFunzionario == null) {
			//Cittadino
			Registrazione = new Button("Registrazione");
			Registrazione.setTitle("RegistrazioneBTN");
			Registrazione.addClickHandler(new ButtonClickHandler());
			hPanel.add(Registrazione);
	
			Login = new Button("Login");
			Login.setTitle("LoginBTN");
			Login.addClickHandler(new ButtonClickHandler());
			hPanel.add(Login);
		}else if(sessioneUtente != null) {
				//Utente Registrato
				NuovaLista = new Button("Crea Lista");
				NuovaLista.setTitle("NuovaListaBTN");
				NuovaLista.addClickHandler(new ButtonClickHandler());
				hPanel.add(NuovaLista);
			}else if(sessioneFunzionario != null) {
				//Funzionario
				NuovaElezione = new Button("Crea Elezione");
				NuovaElezione.setTitle("ElezioneBTN");
				NuovaElezione.addClickHandler(new ButtonClickHandler());
				hPanel.add(NuovaElezione);
				
				AmmettiLista = new Button("Ammetti Liste");
				AmmettiLista.setTitle("AmmettiBTN");
				AmmettiLista.addClickHandler(new ButtonClickHandler());
				hPanel.add(AmmettiLista);
			}else if(isAdmin){
			//Admin
			Nomina = new Button("Nomina");
			Nomina.setTitle("NominaBTN");
			Nomina.addClickHandler(new ButtonClickHandler());
			hPanel.add(Nomina);
		}
		if(sessioneUtente != null || sessioneFunzionario != null) {
			Vota = new Button("Vota");
			Vota.setTitle("VotaBTN");
			Vota.addClickHandler(new ButtonClickHandler());
			hPanel.add(Vota);
			
			Profilo = new Button("Profilo");
			Profilo.setTitle("ProfiloBTN");
			Profilo.addClickHandler(new ButtonClickHandler());
			hPanel.add(Profilo);
		}
		if(isAdmin || sessioneUtente != null || sessioneFunzionario != null) {
			Logout = new Button("Logout");
			Logout.setTitle("LogoutBTN");
			Logout.addClickHandler(new ButtonClickHandler());
			hPanel.add(Logout);
		}
		
		/*Button eliminaUtentiBTN = new Button("Elimina utenti");
		eliminaUtentiBTN.addClickHandler(new EliminaBTNClickHandler());
		hPanel.add(eliminaUtentiBTN);*/
		
		navbarPanel.add(hPanel);
		
	}
	
	//Rimuove tutti gli elementi nel content panel e aggiunge l'elemento composto specificato come parametro
	private void setupForeground(Composite element) {
		int widgets = contentPanel.getWidgetCount();
		if(widgets > 0)
			for(int i = 0; i < widgets; i++)
				contentPanel.remove(i); 
		if(element != null)
			contentPanel.add(element);
	}
	
	
	private void pulisciLabel() {
		if(errorLabel!= null) {
			contentPanel.remove(errorLabel);
		    errorLabel = null;
		}
		if(successLabel != null) {
			contentPanel.remove(successLabel);
		    successLabel = null;
		}
		if(logoutLabel != null) {
			contentPanel.remove(logoutLabel);
		    logoutLabel = null;
		}
	}
	
	//Logout utente
	private void setLogout() {
		sessioneUtente = null;
		sessioneFunzionario = null;
		isAdmin = false; 
		impostaMenu();
		setupForeground(null);
		logoutLabel = new Label("Logout avvenuto con successo!");
		contentPanel.add(logoutLabel);
	}

	//Elimina tutti gli utenti DB
	private class EliminaBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			contentPanel.clear();
			clientImpl.eliminaUtenti();
		}
	}
	
	//Gestisce i pulsanti
	private class ButtonClickHandler implements ClickHandler{
	   @Override
	   public void onClick(ClickEvent event) {
		    Button clicked = (Button) event.getSource();
			switch(clicked.getTitle()) {
				case "RegistrazioneBTN":
					pulisciLabel();
					registrazione = new Registrazione(clientImpl);
					setupForeground(registrazione);
					break;
				case "LoginBTN": 
					pulisciLabel();
					login = new Login(clientImpl);
					setLogout();
					setupForeground(login);
					break;
				case "NominaBTN":
					pulisciLabel();
					if(nomina == null) {
						nomina = new Nomina(clientImpl);
					}else {
						nomina.onUpdateForeground(sessioneUtente);
					}
					setupForeground(nomina);
					break;
				case "ElezioneBTN":
					pulisciLabel();
					nuovaelezione = new NuovaElezione(clientImpl);
					setupForeground(nuovaelezione);
					break;	
				case "NuovaListaBTN":
					pulisciLabel();
					nuovalista = new NuovaLista(clientImpl, sessioneUtente);
					setupForeground(nuovalista);
					break;
				case "AmmettiBTN":
					pulisciLabel();
					ammettilista = new AmmettiLista(clientImpl);
					setupForeground(ammettilista);
					break;	
				case "VotaBTN":
					pulisciLabel();
					if(sessioneUtente != null) {
						votalista = new VotaLista(clientImpl, sessioneUtente);
					}
					if(sessioneFunzionario != null) {
						votalista = new VotaLista(clientImpl,sessioneFunzionario);
					}
					setupForeground(votalista);
					break;
				case "ProfiloBTN":
					pulisciLabel();
					if(sessioneUtente != null) {
						profilo = new Profilo(clientImpl,sessioneUtente);
					}
					if(sessioneFunzionario != null) {
						profilo = new Profilo(clientImpl,sessioneFunzionario);
					}
					setupForeground(profilo);
					break;
				case "RisultatiBTN":
					pulisciLabel();
					risultati = new Risultati(clientImpl);
					setupForeground(risultati);
					break;
				case "LogoutBTN":
					pulisciLabel();
					profilo = null;
					setLogout();
					break;
		   }
	   }
   }

   
}