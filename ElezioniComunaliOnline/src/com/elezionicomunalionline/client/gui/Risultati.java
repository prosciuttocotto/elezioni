package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Risultato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Risultati extends Composite{
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(2, 2);
	private ListBox elezioneListBox = null;
	private Button visualizza = null;
	private VerticalPanel risultatiEl = null;
	private Label noRisultati = null;
	
	private ClientImplementor clientImpl;
	
	public Risultati(ClientImplementor clientImple ) {
		initWidget(this.vPanel);
		this.clientImpl = clientImple;
		
		clientImpl.getElezioniConcluse();
		vPanel.add(gridPanel);
		
	}
	
	public void setElezioniConcluse(ArrayList<Elezione> elezioniConcluse) {
		Label labelElezioni = new Label("Elezione concluse: ");
		elezioneListBox = new ListBox();
		for (Elezione e: elezioniConcluse) {
			elezioneListBox.addItem(e.getOggetto());
		}
		gridPanel.setWidget(0,0, labelElezioni);
		gridPanel.setWidget(0,1, elezioneListBox);
		
		visualizza = new Button("Visualizza risultati");
		visualizza.addClickHandler(new VisualizzaClickHandler());
		gridPanel.setWidget(1, 0, visualizza);
	}
	
	public void setRisultatiElezione(ArrayList<Risultato> risultati) {
		if(risultati.size()>0) {
			risultatiEl = new VerticalPanel();
			vPanel.add(risultatiEl);
			for(Risultato rs: risultati) {
				risultatiEl.add(new Label(rs.getNome()+ " "+rs.getVoti()));
			}
		}else {
			noRisultati = new Label("Nessuna lista e' stata presentata in questa elezione!");
			vPanel.add(noRisultati);
		}
	}
	
	
	
	private class VisualizzaClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			if(risultatiEl != null) {
				vPanel.remove(risultatiEl);
				risultatiEl = null;
			}
			if(noRisultati != null) {
				vPanel.remove(noRisultati);
				noRisultati = null;
			}
			String e = elezioneListBox.getSelectedItemText();
			clientImpl.risultatiElezione(e);
		}
	}
}
