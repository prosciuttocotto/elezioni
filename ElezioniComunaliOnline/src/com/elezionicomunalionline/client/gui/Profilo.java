package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import javax.swing.JLabel;

import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Profilo extends Composite  {
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(2, 2);
	private ArrayList<ListaElettorale> listaListeCreatore = new ArrayList<>();
	
	private ClientImplementor clientImpl;
	private UtenteRegistrato utente;
	private Funzionario funzionario;
	
	public Profilo(ClientImplementor clientImpl, UtenteRegistrato utenteSessione) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		this.utente = utenteSessione;
		VerticalPanel datiUtente=new VerticalPanel();
		
		datiUtente.add(new Label("INFORMAZIONI UTENTE REGISTRATO"));
		datiUtente.add(new Label("Username: "+utente.getUsername()));
		datiUtente.add(new Label("Password: "+utente.getPassword()));
		datiUtente.add(new Label("NOME: "+utente.getNome()));
		datiUtente.add(new Label("COGNOME: "+utente.getCognome()));
		datiUtente.add(new Label("TELEFONO: "+utente.getTelefono()));
		datiUtente.add(new Label("EMAIL: "+utente.getEmail()));
		datiUtente.add(new Label("CODICE FISCALE: "+utente.getCodiceFiscale()));
		datiUtente.add(new Label("INDIRIZZO: "+utente.getIndirizzo()));
		//dati documento d'identita
		datiUtente.add(new Label("TIPO DOCUMENTO: "+utente.getDocumentoIdendita()));
		datiUtente.add(new Label("NUMERO DOCUMENTO: "+utente.getNumeroDoc()));
		datiUtente.add(new Label("COMUNE RILASCIO: "+utente.getComuneDoc()));
		datiUtente.add(new Label("DATA RILASCIO: "+utente.getDataRilascio()));
		datiUtente.add(new Label("DATA SCADENZA: "+utente.getDataScadenza()));

		gridPanel.setWidget(0, 0, datiUtente);
		vPanel.add(gridPanel);
		clientImpl.getListeElettoraliCreatore(utenteSessione);
	}
	
	public Profilo(ClientImplementor clientImpl, Funzionario utenteSessione) {
		initWidget(this.vPanel);
		this.clientImpl = clientImpl;
		this.funzionario = utenteSessione;
		VerticalPanel datiUtente=new VerticalPanel();
		
		datiUtente.add(new Label("INFORMAZIONI FUNZIONARIO"));
		datiUtente.add(new Label("Username: "+funzionario.getUsername()));
		datiUtente.add(new Label("Password: "+funzionario.getPassword()));
		datiUtente.add(new Label("NOME: "+funzionario.getNome()));
		datiUtente.add(new Label("COGNOME: "+funzionario.getCognome()));
		datiUtente.add(new Label("TELEFONO: "+funzionario.getTelefono()));
		datiUtente.add(new Label("EMAIL: "+funzionario.getEmail()));
		datiUtente.add(new Label("CODICE FISCALE: "+funzionario.getCodiceFiscale()));
		datiUtente.add(new Label("INDIRIZZO: "+funzionario.getIndirizzo()));
		//dati documento d'identita
		datiUtente.add(new Label("TIPO DOCUMENTO: "+funzionario.getDocumentoIdendita()));
		datiUtente.add(new Label("NUMERO DOCUMENTO: "+funzionario.getNumeroDoc()));
		datiUtente.add(new Label("COMUNE RILASCIO: "+funzionario.getComuneDoc()));
		datiUtente.add(new Label("DATA RILASCIO: "+funzionario.getDataRilascio()));
		datiUtente.add(new Label("DATA SCADENZA: "+funzionario.getDataScadenza()));

		gridPanel.setWidget(0, 0, datiUtente);
		vPanel.add(gridPanel);
	}
	
	public void setListeElettoraliCreatore(ArrayList<ListaElettorale> listeCreatore) {
		VerticalPanel listeLabel = new VerticalPanel();
		vPanel.add(listeLabel);
		String trattini = "-------------------------------";
		Label lbl = new Label("LISTE PRESENTATE:");
		Label lbl2 = new Label(trattini);
		listeLabel.add(lbl);
		listeLabel.add(lbl2);
		for(ListaElettorale lista : listeCreatore) {
			listeLabel.add(new Label("nome:" + lista.getNome())); 
			listeLabel.add(new Label("stato:" + lista.getStato())); 
			listeLabel.add(new Label("sindaco:" + lista.getSindaco().getUsername()));
			listeLabel.add(new Label("elezione:" + lista.getElezione().getOggetto()));
			listeLabel.add(new Label(trattini));
		}
	
	}

	
}