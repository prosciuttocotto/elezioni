package com.elezionicomunalionline.client.gui;

import java.util.ArrayList;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.service.ClientImplementor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class NuovaLista extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private Grid gridPanel = new Grid(10, 2);
	
	private ArrayList<ListaElettorale> listaListe = new ArrayList<>();
	
	private ListBox elezioneListBox = null;
	private ListBox sindacoListBox = null;
	private ArrayList<UtenteRegistrato> sindacoEcandidati = null;
	private ArrayList<UtenteRegistrato> listaCandidati = new ArrayList<>();
	private ArrayList<UtenteRegistrato> candidatiSelezionati = new ArrayList<>();
	private TextBox nomeLista = null;
	private TextBox simboloLista = null;
	private Button continuaLista = null;
	private Button continuaLista2 = null;
	private Button creaLista = null;
	
	private Label errorLabel;
	private Label successLabel;
	private Label newNuovaLista;
	
	private Button listeBTN;
	private Button eliminaListeBTN;
	
	private UtenteRegistrato utente;
	private ClientImplementor clientImpl;
	
	public NuovaLista(ClientImplementor clientImpl, UtenteRegistrato utente) {
		initWidget(this.vPanel);
		this.utente = utente;
		this.clientImpl = clientImpl;
		
		
		clientImpl.getElezioniUtente(utente);
		
		vPanel.add(new Label("Crea una lista: "));
		Label labelNome = new Label("Nome: ");
		nomeLista = new TextBox();
		nomeLista.getElement().setPropertyString("placeholder", "Nome lista");
		gridPanel.setWidget(1, 0, labelNome);
		gridPanel.setWidget(1, 1, nomeLista);
		
		Label labelSimbolo = new Label("Simbolo: ");
		simboloLista = new TextBox();
		simboloLista.getElement().setPropertyString("placeholder", "Descrizione simbolo lista");
		gridPanel.setWidget(2, 0, labelSimbolo);
		gridPanel.setWidget(2, 1, simboloLista);
		
		listeBTN = new Button("Stampa Liste");
		listeBTN.addClickHandler(new listeBTNClickHandler());
		
		eliminaListeBTN = new Button("Elimina Liste");
		eliminaListeBTN.addClickHandler(new eliminaListeBTNClickHandler());
		
		vPanel.add(gridPanel);
		vPanel.add(listeBTN);
		//vPanel.add(eliminaListeBTN);
		
		errorLabel = new Label("");
		vPanel.add(errorLabel);
		successLabel = new Label("");
		vPanel.add(successLabel);
		
		//menu a tendina con le elezioni in cui l'utente non ha ancora presentato una lista
		
		//menu a tendina con il nome degli utenti registrati che possono partecipare alla lista come SINDACO 
		//checkbox con gli utenti registrati che posso partecipare alla lista (GLI UTENTI REGISTRATI GIA PRESENTI IN UNA LISTA DI QUESTA ELEZIONE NON POSSONO PARTECIPARE)
	}
	
	public void setElezioneNuovaLista(ArrayList<Elezione> elezioni) {
		Label labelElezioni = new Label("Elezione: ");
		elezioneListBox = new ListBox();
		for (Elezione e: elezioni) {
			elezioneListBox.addItem(e.getOggetto());
		}
		gridPanel.setWidget(0,0, labelElezioni);
		gridPanel.setWidget(0,1, elezioneListBox);
		
		
		continuaLista = new Button("Seleziona Sindaco");
		continuaLista.setTitle("SelezionaSindaco");
		gridPanel.setWidget(3,0 , continuaLista);
		continuaLista.addClickHandler(new ButtonClickHandler());
		//chiama il metodo per modificare la lista del sindaco e dei componenti della lista
		
	}
	
	public void setSindacoNuovaLista(ArrayList<UtenteRegistrato> sindaci) {
		Label labelSindaco = new Label("Sindaco: ");
		sindacoListBox = new ListBox();
		sindacoEcandidati = new ArrayList<>();
		sindacoEcandidati.addAll(sindaci);
		for (UtenteRegistrato u: sindaci) {
			sindacoListBox.addItem(u.getUsername()+" "+u.getNome()+ " "+u.getCognome());
		}
		gridPanel.setWidget(4, 0, labelSindaco);
		gridPanel.setWidget(4, 1, sindacoListBox);
		
		continuaLista2 = new Button("Seleziona Candidati");
		continuaLista2.setTitle("SelezionaCandidati");
		gridPanel.setWidget(5, 0, continuaLista2);
		continuaLista2.addClickHandler(new ButtonClickHandler());
	}
	
	public void setCandidatiLista(String sindaco) {
		String[] part = sindaco.split(" ");
		VerticalPanel candidatiCheckBoxes = new VerticalPanel();
		VerticalPanel datiCandidati = new VerticalPanel();
		Label candidatiLabel = new Label("Candidati: ");
		
		creaLista= new Button("Crea Lista");
		creaLista.setTitle("CreaLista");
		creaLista.addClickHandler(new ButtonClickHandler());
		
		int k = 0;
		for(UtenteRegistrato utente: sindacoEcandidati) {
			if(!utente.getUsername().equals(part[0])) {
				CheckBox cb = new CheckBox();
				cb.setTitle(Integer.toString(k));
				listaCandidati.add(k, utente);
				cb.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						UtenteRegistrato candidato = listaCandidati.get(Integer.parseInt(((CheckBox)event.getSource()).getTitle()));
						if(((CheckBox)event.getSource()).getValue()) {
							candidatiSelezionati.add(candidato);
						}else {
							candidatiSelezionati.remove(candidato);
						}
					}
				});
				candidatiCheckBoxes.add(cb);
				datiCandidati.add(new Label(utente.getUsername() + " " +utente.getNome() + " " + utente.getCognome()));
				k++;
			}
		}
		
		gridPanel.setWidget(6, 0, candidatiLabel);
		gridPanel.setWidget(7, 0, candidatiCheckBoxes);
		gridPanel.setWidget(7, 1, datiCandidati);
		
		gridPanel.setWidget(8, 0, creaLista);
		
	}
	
	public void setListeElettorali(ArrayList<ListaElettorale> liste) {
		VerticalPanel l = new VerticalPanel();
		for(ListaElettorale lista : liste) {
			listaListe.add(lista);
			l.add(new Label(lista.getNome() + " " + lista.getStato())); 
		}
		gridPanel.setWidget(9, 0, l);
	}
	
	private class listeBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			clientImpl.getListeElettorali();
		}
	}
	
	private void editErrorLabel(String txt) {
		errorLabel.setText(txt);	
	}
	private void editSuccessLabel(String txt) {
		successLabel.setText(txt);
	}
	
	private class eliminaListeBTNClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			clientImpl.eliminaListe();
		}
	}
	
	//Pulisce il contenuto delle labels
	private void resetLabels() {
		editErrorLabel("");
		editSuccessLabel("");
	}
	
	private void setLayout() {
		newNuovaLista = new Label("Per inserire un'altra lista, clicca sul pulsante Crea Lista!");
		vPanel.remove(gridPanel);
		vPanel.remove(new Label("Crea una lista: "));
		vPanel.remove(listeBTN);
		vPanel.remove(eliminaListeBTN);
		vPanel.add(newNuovaLista);
	}
	
	private class ButtonClickHandler implements ClickHandler{
		 @Override
		   public void onClick(ClickEvent event) {
			 resetLabels();
			 Button clicked = (Button) event.getSource();
			 switch(clicked.getTitle()) {
			 case "SelezionaSindaco": 
				 String elezione = elezioneListBox.getSelectedItemText();
				 if(!elezione.equals("")) {
					 clientImpl.getSindacoElezione(elezione);
				 }
				 break;
			 case "SelezionaCandidati":
				 String sindacoString = sindacoListBox.getSelectedItemText();
				 setCandidatiLista(sindacoString);
				 break;
			 case "CreaLista":
				 //controllare che i dati siano giusti
				 String el = elezioneListBox.getSelectedItemText();
				 String nome = nomeLista.getText();
				 String simbolo = simboloLista.getText();
				 
				 String sindaco = sindacoListBox.getSelectedItemText();
				 String[] part = sindaco.split(" ");
				 int indiceUtenteSindaco = -1;
				 for(int i=0;  i<sindacoEcandidati.size(); i++) {
					 if(sindacoEcandidati.get(i).getUsername().equals(part[0])) {
						 indiceUtenteSindaco = i;
					 }
				 }
				 
				 ArrayList<String> controllaCampi = new ArrayList<>();
				 String err = "";
				 if(el.equals("")) { controllaCampi.add("elezione"); }
				 if(nome.equals("")) { controllaCampi.add("nome"); }
				 if(simbolo.equals("")) { controllaCampi.add("simbolo"); }
				 if(indiceUtenteSindaco == -1) { controllaCampi.add("sindaco"); }
				 
				 if(controllaCampi.size() > 0) {
					 err += "Non hai riempito tutti i campi!";
				 }else {
					 clientImpl.creaListaElettorale(el,nome,simbolo,sindacoEcandidati.get(indiceUtenteSindaco), candidatiSelezionati, utente); 
					 setLayout();
				 }
				 
				 editErrorLabel(err);
				 break;			 
			 }
			 
		 }
	}
}