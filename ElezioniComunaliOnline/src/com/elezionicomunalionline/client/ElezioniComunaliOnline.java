package com.elezionicomunalionline.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.elezionicomunalionline.client.service.ClientImplementor;

public class ElezioniComunaliOnline implements EntryPoint {
	
	public void onModuleLoad() {
		ClientImplementor clientImpl = new ClientImplementor(GWT.getModuleBaseURL() + "standardservice");
		RootPanel.get().add(clientImpl.getMainGUI());
	}
	
}

