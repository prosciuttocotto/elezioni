package com.elezionicomunalionline.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletContext;

import com.elezionicomunalionline.client.model.Admin;
import com.elezionicomunalionline.client.model.Cittadino;
import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.Risultato;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;
import com.elezionicomunalionline.client.service.StandardService;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class StandardServiceImplementor extends RemoteServiceServlet implements StandardService {

	//Registra un nuovo utente
	@Override
	public boolean registraUtente(UtenteRegistrato utenteregistrato) {
		if(!esisteUtente(utenteregistrato)) {
			DB db = getDB();
			Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
			long hash = (long) utenteregistrato.getUsername().hashCode();
			utenti.put(hash, utenteregistrato);
			db.commit();
			return true;
		} else {
			return false;
		}
	}
	
	//Elimina tutti gli utenti (utenti registrati e funzionari)
	@Override
	public void eliminaUtenti() {
		DB db = getDB();
		Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
		Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
		Map<Long, Voto> voti = db.getTreeMap("voti");
		utenti.clear();
		funzionari.clear();
		voti.clear();
		db.commit();
	}
	
	//Restituisce un ArrayList di utenti registrati
	@Override
	public ArrayList<UtenteRegistrato> getUtenti() {
		DB db = getDB();
		ArrayList<UtenteRegistrato> listaUtenti = new ArrayList<>();
		Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
		for(Map.Entry<Long, UtenteRegistrato> utenteregistrato : utenti.entrySet())
			if(utenteregistrato.getValue() instanceof UtenteRegistrato)
				listaUtenti.add((UtenteRegistrato) utenteregistrato.getValue());
			return listaUtenti;
	}
		
	//Restituisce un ArrayList di Funzionari
	@Override
	public ArrayList<Funzionario> getFunzionari() {
		DB db = getDB();
		ArrayList<Funzionario> listaFunzionari = new ArrayList<>();
		Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
		for(Map.Entry<Long, Funzionario> funzionario : funzionari.entrySet())
			if(funzionario.getValue() instanceof Funzionario)
				listaFunzionari.add((Funzionario) funzionario.getValue());
		return listaFunzionari;
	}
	
	//Login di un utente
	@Override
	public Cittadino login(String username, String password) {
		UtenteRegistrato utente = loginUtente(username, password);
		Admin admin = getAdmin(username, password);
		Funzionario funzionario = loginFunzionario(username, password);
		if(utente == null) {
			if(admin == null) {
				if(funzionario == null) {
					return null;
				}else {
					return funzionario;
				}
			}else {
				return admin;
			}
		}else {
			return utente;
		}
	}
	
	//Registra funzionario
	@Override
	public boolean nominaFunzionario(UtenteRegistrato utenteregistrato) {
		if(esisteUtente(utenteregistrato)) {
			DB db = getDB();
			Funzionario funzionario = new Funzionario(utenteregistrato.getUsername(), utenteregistrato.getNome(), 
					utenteregistrato.getCognome(), utenteregistrato.getTelefono(), utenteregistrato.getPassword(), 
					utenteregistrato.getEmail(),utenteregistrato.getCodiceFiscale(), utenteregistrato.getIndirizzo(),
					utenteregistrato.getDocumentoIdendita(), utenteregistrato.getNumeroDoc(), utenteregistrato.getComuneDoc(),
					utenteregistrato.getDataRilascio(),utenteregistrato.getDataScadenza());
			Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
			long hash = (long) funzionario.getUsername().hashCode();
			funzionari.put(hash, funzionario);
			if(eliminaUtenteRegistrato(utenteregistrato)) {
				//System.out.println("Utente registrato eliminato correttamente");
			}
			db.commit();
			//System.out.println("Funzionario inserito correttamente");
			return true;
		}else {
			//System.out.println("Funzionario non inserito");
			return false;
		}
	}
	
	//Revoca un funzionario
	@Override
	public boolean revocaFunzionario(Funzionario funzionario) {
		if(esisteFunzionario(funzionario)) {
			DB db = getDB();
			UtenteRegistrato utenteregistrato = new UtenteRegistrato(funzionario.getUsername(), funzionario.getNome(), 
					funzionario.getCognome(), funzionario.getTelefono(), funzionario.getPassword(), 
					funzionario.getEmail(),funzionario.getCodiceFiscale(), funzionario.getIndirizzo(),
					funzionario.getDocumentoIdendita(), funzionario.getNumeroDoc(), funzionario.getComuneDoc(),
					funzionario.getDataRilascio(),funzionario.getDataScadenza());
			Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
			long hash = (long) utenteregistrato.getUsername().hashCode();
			utenti.put(hash, utenteregistrato);
			if(eliminaFunzionario(funzionario)) {
				System.out.println("Funzionario eliminato correttamente");
			}
			db.commit();
			System.out.println("Utente registrato inserito correttamente");
			return true;
		}else {
			System.out.println("Utente registrato non inserito");
			return false;
		}
	}
	
	//Crea Nuova Elezione
	@Override
	public boolean creaElezione(Elezione elezione) {
		if(!esisteElezione(elezione)) {
			DB db = getDB();
			Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
			long hash = (long) elezione.getOggetto().hashCode();
			elezioni.put(hash, elezione);
			db.commit();
			return true;
		} else {
			return false;
		}
	}
	
	//Restituisce un ArrayList di tutte le Elezioni
	@Override
	public ArrayList<Elezione> getElezioni() {
		DB db = getDB();
		ArrayList<Elezione> listaElezioni = new ArrayList<>();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet())
			listaElezioni.add(elezione.getValue());
		return listaElezioni;
	}
	
	//Restituisce un ArrayList delle elezioni in corso
	@Override
	public ArrayList<Elezione> getElezioniInCorso(String username) {
		DB db = getDB();
		ArrayList<Elezione> listaElezioni = new ArrayList<>();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		Date oggi = getOggi();
		for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet()) {
			if(elezione.getValue().getDataFine().after(oggi)&& elezione.getValue().getDataInizio().before(oggi) ) {
				if(!esisteVoto(username,elezione.getValue())) {
					listaElezioni.add(elezione.getValue());
				}
				//CONTROLLARE ANCHE L'ORAAAA
			}
		}
		return listaElezioni;
	}
	
	public boolean esisteVoto(String username, Elezione elezione) {
		ArrayList<Voto> voti = new ArrayList<>();
		voti.addAll(getVoti());
		for(Voto v:voti) {
			if(v.getElezione().getOggetto().equals(elezione.getOggetto()) && v.getVotante().equals(username)) {
				return true;
			}
		}
		return false;
	}
	
	public ListaElettorale getSingolaListaElettorale(String nomeLista) {
		ArrayList<ListaElettorale> listeElettorali = new ArrayList<>();
		listeElettorali.addAll(getListeElettorali());
		for (int i=0; i<listeElettorali.size(); i++) {
			if(listeElettorali.get(i).getNome().equals(nomeLista)) {
				return listeElettorali.get(i);
			}
		}
		return null;
	}
	
	public UtenteRegistrato getSingoloUtente(String nomeUtente) {
		ArrayList<UtenteRegistrato> utenti = new ArrayList<>();
		utenti.addAll(getUtenti());
		for (int i=0; i<utenti.size(); i++) {
			if(utenti.get(i).getUsername().equals(nomeUtente)) {
				return utenti.get(i);
			}
		}
		return null;
	}
	
	
	@Override
	public boolean votaConCandidato(String elezione, String lista, String candidato, String votante) {
		//System.out.println("Sono entrata in vota con candidato");
		Elezione e = getSingolaElezione(elezione);
		ListaElettorale l = getSingolaListaElettorale(lista);
		UtenteRegistrato u = getSingoloUtente(candidato);
		Voto voto = new Voto(l,e,u,votante);
		DB db = getDB();
		Map<Long, Voto> voti = db.getTreeMap("voti");
		String hashCode = elezione + " " +votante;
		long hash = (long) hashCode.hashCode();
		voti.put(hash,voto);
		db.commit();
		//System.out.println("Voto inserito nel DB "+voto.getVotante()+ " "+ voto.getListaVotata().getNome()+" " +voto.getElezione().getOggetto());
		return true;
		
	}
	
	@Override
	public boolean votaSenzaCandidato(String elezione, String lista, String votante) {
		Elezione e = getSingolaElezione(elezione);
		ListaElettorale l = getSingolaListaElettorale(lista);
		Voto voto = new Voto(l,e,votante);
		
		DB db = getDB();
		Map<Long, Voto> voti = db.getTreeMap("voti");
		String hashCode = elezione + " " +votante;
		long hash = (long) hashCode.hashCode();
		voti.put(hash,voto);
		db.commit();
		return true;
		
	}
	
	//Restituisce tutti i voti inseriti nel database
	@Override
	public ArrayList<Voto> getVoti(){
		DB db = getDB();
		ArrayList<Voto> tuttiVoti = new ArrayList<>();
		Map<Long, Voto> voti = db.getTreeMap("voti");
		for(Map.Entry<Long, Voto> voto : voti.entrySet()) {
			if(voto.getValue() instanceof Voto) {
				tuttiVoti.add((Voto) voto.getValue());
			}
		}
		return tuttiVoti;
	}
	
	//Restituisce un arraylist di listeElettorali dove l'utente � il creatore della lista
	public ArrayList<ListaElettorale> getListeElettoraliCreatore(UtenteRegistrato utente){
		ArrayList<ListaElettorale> liste = new ArrayList<>();
		liste.addAll(getListeElettoraliServer());
		ArrayList<ListaElettorale> listeCreatore = new ArrayList<>();
		for(int i=0; i<liste.size(); i++)
			if(liste.get(i).getCreatoreLista().getUsername().equals(utente.getUsername()))
				listeCreatore.add(liste.get(i));
		return listeCreatore;
	}
	
	//Restituisce un ArrayList delle elezioni non ancora iniziate
	public ArrayList<Elezione> getElezioniFuture() {
		DB db = getDB();
		ArrayList<Elezione> listaElezioni = new ArrayList<>();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		Date oggi = getOggi();
		for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet()) {
			if(elezione.getValue().getDataInizio().after(oggi) ) {
				//CONTROLLARE ANCHE L'ORAAA
				listaElezioni.add(elezione.getValue());
			}
		}
		return listaElezioni;
	}
	
	//Restituisce un ArrayList delle elezioni concluse
	public ArrayList<Elezione> getElezioniConcluse() {
		DB db = getDB();
		ArrayList<Elezione> listaElezioni = new ArrayList<>();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		Date oggi = getOggi();
		for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet()) {
			if(elezione.getValue().getDataFine().before(oggi) ) {
				//CONTROLLARE ANCHE L'ORAAA
				listaElezioni.add(elezione.getValue());
			}
		}
		return listaElezioni;
	}
		
	//Elimina tutte le elezioni
	@Override
	public void eliminaElezioni() {
		DB db = getDB();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		elezioni.clear();
		db.commit();
	}
	
	// Crea Nuova ListaElettorale
	@Override
	public boolean creaListaElettorale(String elezione, String nome, String simbolo, UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati, UtenteRegistrato creatore) {
		ListaElettorale listaElettorale = new ListaElettorale(getSingolaElezione(elezione),nome,simbolo,sindaco,candidati,creatore);
		if (!esisteListaElettorale(listaElettorale)) {
			DB db = getDB();
			Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
			long hash = (long) listaElettorale.getNome().hashCode();
			listeElettorali.put(hash, listaElettorale);
			db.commit();
			return true;
		} else {
			return false;
		}
	}
	
	//Approva Lista
	@Override
	public boolean approvaListaElettorale(ArrayList<ListaElettorale> listaElettorale) {
			DB db = getDB();
			boolean approvata = false; 
			Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
			for(int i=0; i< listaElettorale.size(); i++) {
				for(Map.Entry<Long, ListaElettorale> lista : listeElettorali.entrySet()) {
					if(lista.getValue().getNome().equals(listaElettorale.get(i).getNome())) {
						lista.getValue().setStato("Approvata");
						ListaElettorale listaelettorale = lista.getValue();
						long hashCode = (long) lista.getValue().getNome().hashCode();
						listeElettorali.remove(hashCode);
						listeElettorali.put(hashCode,listaelettorale);
						//System.out.println("Stato aggiornato lista "+lista.getValue().getNome()+ " "+lista.getValue().getStato());
						approvata = true;
					}
				}
			}
			db.commit();
			return approvata;
	}
	
	//Rigetta Lista
	@Override
	public boolean revocaListaElettorale(ArrayList<ListaElettorale> listaElettorale) { 
		DB db = getDB();
		boolean rigettata = false;
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali"); 
		for(int i=0; i< listaElettorale.size(); i++) {
			for(Map.Entry<Long, ListaElettorale> lista : listeElettorali.entrySet()) {
				if(lista.getValue().getNome().equals(listaElettorale.get(i).getNome())) {
					lista.getValue().setStato("Rigettata");
					ListaElettorale listaelettorale = lista.getValue();
					long hashCode = (long) lista.getValue().getNome().hashCode();
					listeElettorali.remove(hashCode);
					listeElettorali.put(hashCode,listaelettorale);
					//System.out.println("Stato aggiornato lista "+lista.getValue().getNome()+ " "+lista.getValue().getStato());
					rigettata = true;
				}
			}
		}
		db.commit();
		return rigettata;
}
	
	
	// Restituisce un ArrayList di ListeElettorali
	@Override
	public ArrayList<ListaElettorale> getListeElettorali() {
		DB db = getDB();
		ArrayList<ListaElettorale> listaListeElettorali = new ArrayList<>();
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
		for (Map.Entry<Long, ListaElettorale> listaElettorale : listeElettorali.entrySet()) {
			listaListeElettorali.add(listaElettorale.getValue());
		}
		return listaListeElettorali;
	}
	
	
	@Override
	public ArrayList<ListaElettorale> getListeElettoraliPendenti(){
		DB db = getDB();
		ArrayList<ListaElettorale> listaListeElettorali = new ArrayList<>();
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
		for (Map.Entry<Long, ListaElettorale> listaElettorale : listeElettorali.entrySet()) {
			if(listaElettorale.getValue().getStato().equals("Pendente")) {
				listaListeElettorali.add(listaElettorale.getValue());
			}
		}
		return listaListeElettorali; 
	}
	
	
/******/
	
	//Restituisce un ArrayList di Elezioni (no override)
	public ArrayList<Elezione> getElezioniServer() {
	  DB db = getDB();
	  ArrayList<Elezione> listaElezioni = new ArrayList<>();
	  Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
	  for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet())
		  listaElezioni.add(elezione.getValue());
	  return listaElezioni;
	}
	
	//Restituisce un ArrayList di ListeElettorali (no override)
	public ArrayList<ListaElettorale> getListeElettoraliServer() {
		DB db = getDB();
		ArrayList<ListaElettorale> listaListeElettorali = new ArrayList<>();
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
		for (Map.Entry<Long, ListaElettorale> listaElettorale : listeElettorali.entrySet()) {
			listaListeElettorali.add(listaElettorale.getValue());
		}
    	return listaListeElettorali;
	}
		
	//Restituisce le liste elettorali di una elezione (no Override)
	public ArrayList<ListaElettorale> getListeElettoraliElezione(Elezione elezione){
		ArrayList<ListaElettorale> liste = new ArrayList<>();
		liste.addAll(getListeElettoraliServer());
		ArrayList<ListaElettorale> listeElezione = new ArrayList<>();
		for(int i=0; i<liste.size(); i++) {
			if(liste.get(i).getElezione().getOggetto().equals(elezione.getOggetto())) {
				listeElezione.add(liste.get(i));
			}
		}
		return listeElezione;
	}
	
	// Restituisce le liste di una elezione
	@Override
	public ArrayList<ListaElettorale> getListeElettoraliElezione(String elezione){
		ArrayList<ListaElettorale> liste = new ArrayList<>();
		liste.addAll(getListeElettoraliServer());
		ArrayList<ListaElettorale> listeElezione = new ArrayList<>();
		for(int i=0; i<liste.size(); i++) {
			if(liste.get(i).getElezione().getOggetto().equals(elezione)) {
				listeElezione.add(liste.get(i));
			}
		}
		return listeElezione;
	}
	
	
	
	
	//Restituisce true se un utente � presente in una lista e false altrimenti
	public boolean trovaUtenteInLista(ListaElettorale lista, UtenteRegistrato utente) {
		if(lista.getSindaco().getUsername().equals(utente.getUsername())) {
			return true;
		}else {
			ArrayList<UtenteRegistrato> componentiLista = new ArrayList<>();
			componentiLista.addAll(lista.getComponentiLista());
			for(int i=0; i<componentiLista.size(); i++) {
				if(componentiLista.get(i).getUsername().equals(utente.getUsername())) {
					return true;
				}
			}		
		}
		return false;
	}
	
	//Restituisce true se un utente � presente in una elezione
	public boolean trovaUtenteInElezione(Elezione elezione, UtenteRegistrato utente) {
		ArrayList<ListaElettorale> listeElezione = new ArrayList<>();
		listeElezione.addAll(getListeElettoraliElezione(elezione));
		for(int i=0; i<listeElezione.size(); i++){
			if(trovaUtenteInLista(listeElezione.get(i), utente)) {
				return true;
			}
		}
		return false;
	}
		
	//Restituisce tutte le elezioni in cui l'utente non � presente in nessuna lista
	@Override
	public ArrayList<Elezione> getElezioniUtente(UtenteRegistrato utente){
		ArrayList<Elezione> elezioni = new ArrayList<>();
		ArrayList<Elezione> listaElezioni = new ArrayList<>();
		listaElezioni.addAll(getElezioniServer());
		for(int i=0; i<listaElezioni.size(); i++) {
			if(!trovaUtenteInElezione(listaElezioni.get(i), utente)) {
				elezioni.add(listaElezioni.get(i));
			}
		}
		return elezioni;
	}
	
	public Elezione getSingolaElezione(String oggettoElezione) {
		ArrayList<Elezione> elezioni = new ArrayList<>();
		elezioni.addAll(getElezioni());
		for (int i=0; i<elezioni.size(); i++) {
			if(elezioni.get(i).getOggetto().equals(oggettoElezione)) {
				return elezioni.get(i);
			}
		}
		return null;
	}
	
	//Restituisce la lista di tutti gli utenti che possono partecipare come Sindaco in una determinata elezione
	@Override
	public ArrayList<UtenteRegistrato> getSindacoElezione(String oggettoelezione){
		ArrayList<UtenteRegistrato> listaUtentiElezione = new ArrayList<>();
		ArrayList<UtenteRegistrato> listaUtenti = new ArrayList<>();
		Elezione elezione = getSingolaElezione(oggettoelezione);
		listaUtenti.addAll(getUtenti());
		
		for(int i=0; i<listaUtenti.size(); i++) {
			if(!trovaUtenteInElezione(elezione,listaUtenti.get(i))) {
				listaUtentiElezione.add(listaUtenti.get(i));
			}
		}
		return listaUtentiElezione;
	}
	
	//Elimina tutte le liste
	@Override
	public void eliminaListe() {
		DB db = getDB();
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
		listeElettorali.clear();
		db.commit();
	}
	
	//Restituisce gli utenti di una lista
	@Override
	public ArrayList<UtenteRegistrato> getCandidatiLista(String nomeLista, String elezione){
		//getListeElettoraliElezione
		ArrayList<ListaElettorale> listeElettorali = new ArrayList<>();
		listeElettorali.addAll(getListeElettoraliElezione(elezione));
		for(ListaElettorale lista: listeElettorali) {
			if(lista.getNome().equals(nomeLista)) {
				return lista.getComponentiLista();
			}
		}
		return null;
	}

/******/
	
	//Restituisce il numero di voti totale di una lista in una elezione 
	public int numeroVotiLista(String elezione, String nomeLista) {
		int numeroVoti = 0;
		ArrayList<Voto> voti = new ArrayList<>();
		voti.addAll(getVoti());
		for(Voto voto:voti) {
			if(voto.getElezione().getOggetto().equals(elezione) && 
					voto.getListaVotata().getNome().equals(nomeLista)) {
				numeroVoti++;
			}
		}
		return numeroVoti;
	}
			
	//Restituisce il numero di voti totale di un candidato di una lista di una elezione
	public int numeroVotiCandidato(String elezione, String nomeLista, String candidato) {
		int numeroVoti = 0;
		ArrayList<Voto> voti = new ArrayList<>();
		voti.addAll(getVoti());
		for(Voto voto:voti) {
			if(voto.getElezione().getOggetto().equals(elezione) && 
					voto.getListaVotata().getNome().equals(nomeLista)) {
				if(voto.getCandidato() != null) {
					if(voto.getCandidato().getUsername().equals(candidato)) {
						numeroVoti++;
					}
				}
			}
		}
		return numeroVoti;
	}
			
	//Restituisce il risultati di una elezione
	@Override
	public ArrayList<Risultato> risultatiElezione(String oggettoElezione){
		ArrayList<Risultato> risultati = new ArrayList<>();
		ArrayList<ListaElettorale> listeElezione = new ArrayList<>();
		listeElezione.addAll(getListeElettoraliElezione(oggettoElezione));
		int k=0;
		for(int i=0; i<listeElezione.size(); i++) {
			int numeroVoti = numeroVotiLista(oggettoElezione,listeElezione.get(i).getNome());
			String nomeLista = i +" - "+listeElezione.get(i).getNome();
			Risultato ris = new Risultato(nomeLista, numeroVoti);
			risultati.add(ris);
			for(int y=0; y<listeElezione.get(i).getComponentiLista().size(); y++) {
				int votiCandidato = numeroVotiCandidato(oggettoElezione,listeElezione.get(i).getNome(),listeElezione.get(i).getComponentiLista().get(y).getUsername());
				String candidato = listeElezione.get(i).getComponentiLista().get(y).getUsername()+ " "+listeElezione.get(i).getComponentiLista().get(y).getNome()+ " "+listeElezione.get(i).getComponentiLista().get(y).getCognome();
				Risultato r = new Risultato(candidato,votiCandidato);
				risultati.add(r);
			}
		}
		return risultati;
	} 

	//Elimina un utente registrato
	public boolean eliminaUtenteRegistrato(UtenteRegistrato utente) {
		DB db = getDB();
		Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
		long hashCode = (long) utente.getUsername().hashCode();
		if(utenti.containsKey(hashCode)) {
			utenti.remove(hashCode);
			 return true;
		}else {
			return false;
		}
	}
	
	//Elimina un funzionario
	public boolean eliminaFunzionario(Funzionario funzionario) {
		DB db = getDB();
		Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
		long hashCode = (long) funzionario.getUsername().hashCode();
		if(funzionari.containsKey(hashCode)) {
			funzionari.remove(hashCode);
			 return true;
		}else {
			return false;
		}
	}

	//Restituisce l'utente registrato loggato
	public UtenteRegistrato loginUtente(String username, String password) {
		DB db = getDB();
		Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
		long hashCode = (long) username.hashCode();
		if(utenti.containsKey(hashCode)) {
			if(utenti.get(hashCode).getPassword().equals(password)) {
				//System.out.println("login fatto");
				return utenti.get(hashCode);
			}else {
				//System.out.println("login non fatto");
				return null;
			}
		} else {
			//System.out.println("login non fatto 2");
			return null;
		}
	}
	
	//Restituisce il funzionario loggato
	public Funzionario loginFunzionario(String username, String password) {
		DB db = getDB();
		Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
		long hashCode = (long) username.hashCode();
		if(funzionari.containsKey(hashCode)) {
			if(funzionari.get(hashCode).getPassword().equals(password)) {
				//System.out.println("login funzionario fatto");
				return funzionari.get(hashCode);
			}else {
				//System.out.println("login funzionario non fatto");
				return null;
			}
		}else {
			//System.out.println("login funzionario non fatto 2");
			return null;
		}
	}
	
	//Restituisce l'admin
	public Admin getAdmin(String username, String password) {
		DB db = getDB();
		Map<Long, Admin> admin = db.getTreeMap("admin");
		long hashCode = (long) username.hashCode();
		if(admin.containsKey(hashCode)) {
			if(admin.get(hashCode).getPassword().equals(password)) {
				//System.out.println("login admin fatto");
				return admin.get(hashCode);
			}else {
				//System.out.println("login admin non fatto");
				return null;
			}
		}else {
			//System.out.println("login admin non fatto 2");
			return null;
		}
	}
	
	
	
	
	//Controlla se un utente � gi� presente nel database
	private boolean esisteUtente(UtenteRegistrato checkUtente) {
		DB db = getDB();
		Map<Long, UtenteRegistrato> utenti = db.getTreeMap("utenti");
		for(Map.Entry<Long, UtenteRegistrato> utente : utenti.entrySet()) {
			if(utente.getValue() instanceof UtenteRegistrato && ((UtenteRegistrato)utente.getValue()).getUsername().equals(checkUtente.getUsername())) {
				return true;
			}
		}
		return false;
	}
	
	//Controlla se un funzionario � gi� presente nel database
	private boolean esisteFunzionario(Funzionario checkFunzionario) {
		DB db = getDB();
		Map<Long, Funzionario> funzionari = db.getTreeMap("funzionari");
		for(Map.Entry<Long, Funzionario> funzionario : funzionari.entrySet()) {
			if(funzionario.getValue() instanceof Funzionario && ((Funzionario)funzionario.getValue()).getUsername().equals(checkFunzionario.getUsername())) {
				return true;
			}
		}
		return false;
	}
	
	//Controlla se l'admin � gi� presente, e nel caso lo aggiunge
	private void controllaAdmin(DB db) {
		Map<Long, Admin> admin = db.getTreeMap("admin");
		long hashCode = (long) "admin".hashCode();
		if(!admin.containsKey(hashCode))
			admin.put(hashCode, new Admin("admin", "admin"));
	}
	
	//Controlla se un'elezione � gi� presente nel database
	private boolean esisteElezione(Elezione controllaElezione) {
		DB db = getDB();
		Map<Long, Elezione> elezioni = db.getTreeMap("elezioni");
		for(Map.Entry<Long, Elezione> elezione : elezioni.entrySet()) {
			if(elezione.getValue() instanceof Elezione && ((Elezione)elezione.getValue()).getOggetto().equals(controllaElezione.getOggetto())) {
				return true;
			}
		}
		return false;
	}
	
	// Controlla se una lista elettorale � gi� presente nel database
	private boolean esisteListaElettorale(ListaElettorale controllaListaElettorale) {
		DB db = getDB();
		Map<Long, ListaElettorale> listeElettorali = db.getTreeMap("listeElettorali");
		for (Map.Entry<Long, ListaElettorale> listaElettorale : listeElettorali.entrySet()) {
			if (listaElettorale.getValue() instanceof ListaElettorale && ((ListaElettorale) listaElettorale.getValue())
					.getNome().equals(controllaListaElettorale.getNome())) {
				return true;
			}
		}
		return false;
	}
	
	//Restituisce la data di oggi del file oggi.txt
	private Date getOggi() {
		Date res = new Date();
		try {
			DataInputStream input = new DataInputStream(new FileInputStream("oggi.txt"));
			BufferedReader buffer = new BufferedReader(new InputStreamReader(input));
			String riga = "";
			String contenuto = "";
			
			while ((riga = buffer.readLine()) != null) {
				contenuto += riga;
			}
			
			try {
			    res = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(contenuto.trim());
			} catch(ParseException e) {
				System.err.println("Error: " + e.getMessage());
			}
			input.close();
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
				
		return res;
	}
	
	//Restituisce un oggetto database rappresentante la struttura mapDB
		private DB getDB() {
			ServletContext context = this.getServletContext();
			synchronized (context) {
				DB db = (DB) context.getAttribute("DB");
				if (db == null) {
					db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
					context.setAttribute("DB", db);
				}
				controllaAdmin(db);
				return db;
			}
		}

}
