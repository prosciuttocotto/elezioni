package com.elezionicomunalionline.test;

import java.util.ArrayList;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.elezionicomunalionline.client.model.Elezione;
import com.elezionicomunalionline.client.model.Funzionario;
import com.elezionicomunalionline.client.model.ListaElettorale;
import com.elezionicomunalionline.client.model.UtenteRegistrato;
import com.elezionicomunalionline.client.model.Voto;
import com.elezionicomunalionline.client.service.StandardService;
import com.elezionicomunalionline.client.service.StandardServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StandardServiceImplementorTest extends GWTTestCase {
	
	private StandardServiceAsync standardService;
	private ServiceDefTarget target;

	/* setup server */
	private void setServer() {
		standardService = GWT.create(StandardService.class);
		target = (ServiceDefTarget) standardService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "elezionicomunalionline/standardservice");
		delayTestFinish(10000);
	}

	//Test registrazione utente
	@Test
	public void test0registraUtente() {
		setServer();
		
		//Pulisco il DB
		standardService.eliminaUtenti(new DefaultCallback());
		
		UtenteRegistrato utenteTest = new UtenteRegistrato("test", "testpwd", "test", "test", "1", "test@a.a", "1111111111111111", "test", "Passaporto", "1", "test", new Date(), new Date());
		standardService.registraUtente(utenteTest, new TrueCallback());
		standardService.registraUtente(utenteTest, new FalseCallback());
		
		UtenteRegistrato utente1 = new UtenteRegistrato("a", "aaaaaa", "a", "a", "1", "aa@aa.aa", "2222222222222222", "a", "Passaporto", "1", "a", new Date(), new Date());
		UtenteRegistrato utente2 = new UtenteRegistrato("b", "bbbbbb", "b", "b", "1", "bb@bb.bb", "3333333333333333", "b", "Passaporto", "1", "b", new Date(), new Date());
		UtenteRegistrato utente3 = new UtenteRegistrato("c", "cccccc", "c", "c", "1", "cc@cc.cc", "4444444444444444", "c", "Passaporto", "1", "c", new Date(), new Date());
		standardService.registraUtente(utente1, new TrueCallback());
		standardService.registraUtente(utente2, new TrueCallback());
		standardService.registraUtente(utente3, new TrueCallback());
	}
	
	//Test login utente
	@Test
	public void test1Login() {
		setServer();
		standardService.login("test", "testpwd", new UtenteCallback());
		standardService.login("test", "Testpwd", new NullCallback());
		standardService.login("", "", new NullCallback());
	}
	
	//Test nomina funzionari
	@Test
	public void test2NominaFunzionario() {
		setServer();
		UtenteRegistrato utenteTest = new UtenteRegistrato("test", "testpwd", "test", "test", "1", "test@a.a", "1111111111111111", "test", "Passaporto", "1", "test", new Date(), new Date());
		UtenteRegistrato utenteFalso = new UtenteRegistrato("no", "nonono", "no", "1", "no", "no@a.a", "0000000000000000", "no", "Passaporto", "1", "no", new Date(), new Date());
		standardService.nominaFunzionario(utenteTest, new TrueCallback());
		standardService.nominaFunzionario(utenteFalso, new FalseCallback());
	}
	
	//Test revoca funzionari
		@Test
		public void test3RevocaFunzionario() {
			setServer();
			Funzionario funzionarioTest = new Funzionario("test", "testpwd", "test", "test", "1", "test@a.a", "1111111111111111", "test", "Passaporto", "1", "test", new Date(), new Date());
			Funzionario funzionarioFalso = new Funzionario("no", "nonono", "no", "1", "no", "no@a.a", "0000000000000000", "no", "Passaporto", "1", "no", new Date(), new Date());
			standardService.revocaFunzionario(funzionarioTest, new TrueCallback());
			standardService.revocaFunzionario(funzionarioFalso, new FalseCallback());
		}
		
	//Test nuova elezione
	@Test
	public void test4CreaElezione() {
		setServer();
		
		//Pulisco il DB
		standardService.eliminaElezioni(new DefaultCallback());
		
		Elezione elezione1 = new Elezione("Elezione1", new Date(System.currentTimeMillis() - 24*60*60*1000), new Date(System.currentTimeMillis() + 24*60*60*1000));
		Elezione elezione2 = new Elezione("Elezione2", new Date(System.currentTimeMillis() - 24*60*60*1000), new Date(System.currentTimeMillis() + 24*60*60*1000));
		standardService.creaElezione(elezione1, new TrueCallback());
		standardService.creaElezione(elezione2, new TrueCallback());
		standardService.creaElezione(elezione1, new FalseCallback());
		standardService.creaElezione(elezione2, new FalseCallback());

	}
	
	//Test nuova lista
	@Test
	public void test5CreaLista() {
		setServer();
		
		//Pulisco il DB
		standardService.eliminaListe(new DefaultCallback());
		
		UtenteRegistrato utente1 = new UtenteRegistrato("a", "aaaaaa", "a", "a", "1", "aa@aa.aa", "2222222222222222", "a", "Passaporto", "1", "a", new Date(), new Date());
		UtenteRegistrato utente2 = new UtenteRegistrato("b", "bbbbbb", "b", "b", "1", "bb@bb.bb", "3333333333333333", "b", "Passaporto", "1", "b", new Date(), new Date());
		UtenteRegistrato utente3 = new UtenteRegistrato("c", "cccccc", "c", "c", "1", "cc@cc.cc", "4444444444444444", "c", "Passaporto", "1", "c", new Date(), new Date());
		
		ArrayList<UtenteRegistrato> utenti = new ArrayList<>();
		utenti.add(utente1); utenti.add(utente2); utenti.add(utente3); 
		
		//Elezione elezione1 = new Elezione("Elezione1", new Date(System.currentTimeMillis() - 24*60*60*1000), new Date(System.currentTimeMillis() + 24*60*60*1000));
		//ListaElettorale lista1 = new ListaElettorale(elezione1, "Lista1", "simbolo", utente1, utenti, utente1);
		
		standardService.creaListaElettorale("Elezione1", "Lista1", "simbolo", utente1,  utenti, utente1, new TrueCallback());
		standardService.creaListaElettorale("Elezione1", "Lista1", "simbolo", utente1,  utenti, utente1, new FalseCallback());
		standardService.creaListaElettorale("Elezione1", "Lista2", "simbolo", utente2,  utenti, utente3, new TrueCallback());
		standardService.creaListaElettorale("Elezione1", "Lista2", "simbolo", utente2,  utenti, utente3, new FalseCallback());
	}
	
	//Test ammetti lista
	@Test
	public void test6AmmettiLista() {
		setServer();
		
		UtenteRegistrato utente1 = new UtenteRegistrato("a", "aaaaaa", "a", "a", "1", "aa@aa.aa", "2222222222222222", "a", "Passaporto", "1", "a", new Date(), new Date());
		UtenteRegistrato utente2 = new UtenteRegistrato("b", "bbbbbb", "b", "b", "1", "bb@bb.bb", "3333333333333333", "b", "Passaporto", "1", "b", new Date(), new Date());
		UtenteRegistrato utente3 = new UtenteRegistrato("c", "cccccc", "c", "c", "1", "cc@cc.cc", "4444444444444444", "c", "Passaporto", "1", "c", new Date(), new Date());
		
		ArrayList<UtenteRegistrato> utenti = new ArrayList<>();
		utenti.add(utente1); utenti.add(utente2); utenti.add(utente3); 
		
		Elezione elezione1 = new Elezione("Elezione1", new Date(System.currentTimeMillis() - 24*60*60*1000), new Date(System.currentTimeMillis() + 24*60*60*1000));
		
		ListaElettorale lista1 = new ListaElettorale(elezione1, "Lista1", "simbolo", utente1, utenti, utente1);
		ListaElettorale lista2 = new ListaElettorale(elezione1, "Lista2", "simbolo", utente2, utenti, utente3);
		
		ArrayList<ListaElettorale> liste = new ArrayList<>();
		liste.add(lista1); liste.add(lista2);
			
		standardService.approvaListaElettorale(liste, new TrueCallback());
	}
	
	//Test vota lista senza candidato
	@Test
	public void test7VotaLista() {
		setServer();
		
		standardService.votaSenzaCandidato("elezione1", "lista1", "utente1", new TrueCallback());
	}
	
	//Test vota lista con candidato
		@Test
		public void test8VotaLista() {
		setServer();
		
		standardService.votaConCandidato("elezione1", "lista1", "utente2", "utente1", new TrueCallback());
	}
	
	@Override
	public String getModuleName() {
		return "com.elezionicomunalionline.ElezioniComunaliOnlineJUnit";
	}

	/* CALLBACK */
	
	private class DefaultCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) { }

		@Override
		public void onSuccess(Object result) { finishTest(); }
	}

	private class TrueCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertTrue("Must be true", (boolean) result);
			finishTest();
		}
	}

	private class FalseCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertFalse("Must be false", (boolean) result);
			finishTest();
		}
	}
	
	private class NullCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertNull("Must be null", result);
			finishTest();
		}
	}

	private class UtenteCallback implements AsyncCallback<Object> {
		@Override
		public void onFailure(Throwable caught) {
			fail("Request failure: " + caught.getMessage());
		}

		@Override
		public void onSuccess(Object result) {
			assertTrue("Must be of type UtenteRegistrato", result instanceof UtenteRegistrato);
			finishTest();
		}
	}
}